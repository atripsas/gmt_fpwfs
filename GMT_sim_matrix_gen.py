import hcipy
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import time
from tqdm import tqdm
import matplotlib
import matplotlib.gridspec as gridspec

#Makes output images large enough for tight_spacing command
plt.rcParams['figure.figsize'] = [20, 10]



class GMT_FPWFS_matrix_gen(object):

    def __init__(self, lam, leakage,res,diag):
        super().__init__()
        self.lam = lam
        self.leakage = leakage
        self.res = res
        self.diag = diag
        return

    def import_gmt_image(self, data_file, file_header = False):
        '''
        Just like the definition says, it imports gmt masks
        Input: 
            data_file: string path to fits file
            default values:
                1: pupil amplitude mask
                2: pupil phase mask
                3: GMT pupil with spiders (not currently used)
        Output:
            2d data array, header is default False
        '''
        if data_file == 1:
            data_file = 'GMT_asymmetric_pupil_amp_gvAPP_high_res.fits'
        if data_file == 2:
            data_file = 'GMT_asymmetric_pupil_phase_gvAPP_high_res.fits'
        if data_file == 3:
            data_file = 'gmt-pupil-1cm.fits'
        return fits.getdata(data_file, header= file_header)

    def imgarr_to_field(self, im):
        '''
        Turns a 2d image array into a HCIpy field and grid
        Input:
            im: 2d array Image file
        Output:
            HCIpy field and grid
        '''
        grid = hcipy.make_pupil_grid(im.shape[0], diameter = 1)
        im_flat = im.flatten()
        flat_field = np.ones(len(im_flat))
        field = hcipy.Field(flat_field, grid)
        field *= im_flat
        return field, grid

    def image_init(self):
        '''
        Quickly imports default images
        '''

        if self.res=='high':    
            amp = self.import_gmt_image(data_file = 1)
            phase = self.import_gmt_image(data_file = 2)
        if self.res=='low':
            amp     = self.import_gmt_image('GMT_asymmetric_pupil_amp_gvAPP_low_res.fits')
            phase   = self.import_gmt_image('GMT_asymmetric_pupil_phase_gvAPP_low_res.fits')
        asym_amp_field, asym_amp_grid = self.imgarr_to_field(amp)
        asym_phase_field, _ = self.imgarr_to_field(phase)

        return amp, phase, asym_amp_field, asym_phase_field, asym_amp_grid

    def log_plot(self,im):
        '''
        Takes the log_10 and normalizes it for plotting.  Only used for plotting
        Input:
            im: 2d array
        Output:
            Normalized log sale image
        '''
        return np.log10(im.intensity / im.intensity.max())

    def diff_plot(self,im_pos,im_neg):
        '''
        Takes the difference in intensities of the positive and negative images
        to get influence function
        Input:
            im_pos: positive image Field
            im_neg: negative image Field
        Output:
            Intensity difference field
        '''
        return ((im_pos.intensity / im_pos.intensity.max()) - 
                (im_neg.intensity / im_neg.intensity.max()))

    def circle_mask(self, im, xc, yc, rcirc):
        '''
        Creates a True/False mask in the shape of a circle in an image
        Inputs:
            im: Image file (2d array)
            xc: x-coordinate
            yc: y-coordinate
            rcirc: radius of the circle in pixels

        Outputs:
            2d array with True, False values.  True within circle values
        '''
        ny, nx = im.shape
        y,x = np.mgrid[0:nx,0:ny]
        r = np.sqrt((x-xc)*(x-xc) + (y-yc)*(y-yc))
        return ( (r < rcirc))
        
    def comb_amp_phase(self,amp,phase):
        '''
        Technically not correct way for propagation.
        Combines the amplitude mask and phase mask using the equation
        I = A * leakage * (exp(i*phi + -i*phi))
        '''
        return amp * ( self.leakage + np.exp(1j*phase) + np.exp(-1j*phase) )

    def segment_selector(self, seg=None, display=False):
        '''
        Isolates a segment of the GMT from the GMT binary mask

        Input:
            im: 2d image array
            seg: is the segment list
            display: If the user needs to see which segment is which, display = True
            then the code will prompt the user to choose a segment
        Output:
            An isolated GMT segment
        '''

            
        if seg == None:
            display = True

        if self.res == 'high':
            im = self.import_gmt_image(data_file=1)
            # gmt_asym_amp = self.import_gmt_image(data_file=1)
            seg_list = [[3000,3000],[3000,5050],[4777,4020],[4777,1987],[3000,975],[1250,1987],[1250,4020]] 
            r_seg = 1015
        if self.res == 'low':
            im = self.import_gmt_image('GMT_asymmetric_pupil_amp_gvAPP_low_res.fits')
            # gmt_asym_amp = self.import_gmt_image('GMT_asymmetric_pupil_phase_gvAPP_low_res.fits')
            seg_list = [[150,150],[150,251],[238,200],[238,99],[150,48],[62,99],[62,200]]
            r_seg = 50

        if display:
            
            plt.imshow(im, origin = 'lower')
            for i in range(len(seg_list)):
                seg = plt.Circle((seg_list[i][0],seg_list[i][1]),r_seg,color='r',fill=False)
                plt.text(seg_list[i][0],seg_list[i][1],str(i+1), color = 'r', fontsize = 20)
                plt.gca().add_artist(seg)
            plt.colorbar()
            plt.show()
            txt = input('Which segment would you like to isolate? (1-7): ')
            seg = int(txt)

        if 1<= seg <=7:
            seg_coord = seg_list[seg-1]
        else:
            txt = input('pick a value between 1 and 7: ')
            seg_coord = seg_list[int(txt)-1]

        seg = self.circle_mask(im,seg_coord[0],seg_coord[1],r_seg) * im

        if display:
            plt.imshow(seg, origin='lower')
            plt.show()
        
        return seg

    def prop_dat_image(self,field,q=None,num_airy=None):
        '''
        Code propogates the image from the pupil plane to the focal plane
        aka props dat image.  Process is time consuming.

        Input:
            Field: vector field generated by HCIpy
            pupil_grid: HCIpy grid from pupil plane
        Output:
            Propagated image from pupil plane to focal plane (HCIpy)
              Does not seem to work properly.  Grid does not follow through
        '''
        if q == None:
            q=4
        if num_airy == None:
            num_airy = 32

        wavef = hcipy.Wavefront(field,1)
        focal_grid = hcipy.make_focal_grid(q,num_airy)
        prop = hcipy.FraunhoferPropagator(field.grid,focal_grid)
        focal_image = prop.forward(wavef)

        # if self.diag:
        #     hcipy.imshow_field(focal_image)

        return focal_image

    def PSF_combine(self,amp_mask,phase_mask,mod):
        '''
        Takes amplitude mask and phase mask and propogates each PSF.  After 
        propagation intensities are added together
        Input:
            amp_mask: 2-D binary image array
            phase_mask: 2-D image array
        Optional:
            mod: 2-D P/T/T manipulation
        Output:
            Propagated image
        '''

        mod = mod.ravel()


        top = amp_mask * np.exp(1j * (mod + phase_mask)) #Top PSF
        leak = amp_mask * self.leakage * np.exp(1j * mod) # Leakage term
        #modified term does not experience -1j, that is to only have a bottom PSF from the phase mask
        bottom = amp_mask * np.exp((-1j * phase_mask) + (1j * mod)) #Bottom PSF

        if self.diag:
            # print(np.max(top))
            plt.clf()
            hcipy.imshow_field(top)
            plt.title('Top PSF pupil plane')
            plt.colorbar()
            plt.show()
            plt.clf()
            hcipy.imshow_field(leak)
            plt.title('Leak PSF pupil plane')
            plt.colorbar()
            plt.show()
            plt.clf()
            hcipy.imshow_field(bottom)
            plt.title('Bottom PSF pupil plane')
            plt.colorbar()
            plt.show()
            plt.clf()
            hcipy.imshow_field(top + leak + bottom)
            plt.title('Combined PSF pupil plane')
            plt.colorbar()
            plt.show()

        prop_top = self.prop_dat_image(top)
        prop_leak = self.prop_dat_image(leak)
        prop_bottom = self.prop_dat_image(bottom)

        int_top = prop_top.intensity
        int_leak = prop_leak.intensity
        int_bottom = prop_bottom.intensity

        total_psf = int_top + int_bottom + int_leak

        total_psf /= np.sum(total_psf)

        if self.diag:
            hcipy.imshow_field(np.log10(total_psf), vmin=-6)
            plt.title('Combined focal plane')
            plt.colorbar()
            plt.show()

        

        return total_psf

    def reference_image(self,display=False, combo = False):
        '''
        Combines amplitude mask and phase mask and propegates the image to the focal
        plane.
        '''
        _, _, asym_amp_field, asym_phase_field, _ = self.image_init()
        gmt_pupil_combo = self.comb_amp_phase(amp=asym_amp_field,
                                                phase=asym_phase_field)

        #uniform mod mask for reference
        mod = np.ones_like(asym_amp_field)



        if display:
            hcipy.imshow_field(self.log_plot(gmt_pupil_combo))
            plt.colorbar()
            plt.show()
        if combo:
            gmt_focal_combo = self.comb_amp_phase(asym_amp_field,asym_phase_field)
        elif combo == False:
            gmt_focal_combo = self.PSF_combine(asym_amp_field, asym_phase_field, mod)
        
        if display:
            hcipy.imshow_field(self.log_plot(gmt_focal_combo))
            plt.colorbar()
            plt.show()
        
        return gmt_focal_combo


    def piston(self, seg, flip=1, amp=None):
        '''
        Applies piston to specified segment
        Input:
            seg: segment 1-7
            flip: modifier for +/- 1
            amp: OPD
        Output:
            Focal plane of applied piston (Field)
        '''

        if amp == None:
            amp = 1

        #import images
        _, _, asym_amp_field, asym_phase_field, _ = self.image_init()

        #Select segment to modify
        single_segment = self.segment_selector(seg = seg)
        #OPD of piston compared to 
        phase = 2. * np.pi * ((flip * amp) / self.lam)

        #Enduce piston to segment
        piston_single_segment = (phase * single_segment)#.ravel()) #+ asym_phase_field


        if self.diag:
            plt.clf()
            plt.imshow(piston_single_segment, origin='lower')
            plt.title('Modified segment')
            plt.colorbar()
            plt.show()

        #Apply pistoned segment to pupil plane and propagate
        pist_comb_focal = self.PSF_combine(asym_amp_field,asym_phase_field,mod=piston_single_segment)
        
        if self.diag:
            plt.clf()
            hcipy.imshow_field(pist_comb_focal)
            plt.title('Combined output focal plane')
            plt.colorbar()
            plt.show()

        return pist_comb_focal, amp



    def tip(self, seg, flip=1, amp=None):
        '''
        Applies tilt to segment (y-axis)
        Input:
            seg: segment number 1-7
            flip: direction modifier (direction of tilt +/-1)
            amp: tilt amplitude
        Output:
            Focal plane of applied tip (Field)
        '''

        if amp == None:
            amp = 1

        #Load fields
        _, _, asym_amp_field, asym_phase_field, _ = self.image_init()

        #Select Segment to modify
        single_segment = self.segment_selector(seg = seg)

        #Generate tip
        tip, _ = np.mgrid[-np.shape(single_segment)[0]/2:np.shape(single_segment)[0]/2,
                            -np.shape(single_segment)[1]/2:np.shape(single_segment)[1]/2]

        #Apply tip to segment
        tip_im = single_segment * tip
        #Make center of segment 0
        tip_im_norm = np.where(tip_im == 0,
                                tip_im != 0,
                                tip_im - np.mean(tip_im[np.nonzero(tip_im)])
                                )
        # make edges +/- 1, ~0.9995
        if self.diag:
            print(tip_im_norm.max())
        # tip_im_norm /= np.std(tip_im_norm[single_segment == 1])#.max()
        tip_im_norm /= tip_im_norm.max()

        if self.diag:
            plt.clf()
            plt.imshow(tip_im_norm,origin='lower')
            plt.title('normalized seg tip')
            plt.colorbar()
            plt.show()

        phase = 2. * np.pi * ((amp * flip) / self.lam)

        #Apply tip to 
        tip_single_seg = (tip_im_norm * phase) #+ asym_phase_field

        if self.diag:
            plt.clf()
            plt.imshow(tip_single_seg, origin='lower')
            plt.colorbar()
            plt.show()

        #combine and propagate image
        tip_comb_focal = self.PSF_combine(asym_amp_field, asym_phase_field, mod = tip_single_seg)

        if self.diag:
            plt.clf()
            hcipy.imshow_field(tip_comb_focal)
            plt.colorbar()
            plt.show()

        return tip_comb_focal, amp

    def tilt(self, seg, flip=1, amp=None):
        '''
        Applies tilt to segment (x-axis).  Same steps as tip, just on different axis
        Input:
            seg: segment number 1-7
            flip: direction modifier
            amp: tilt amplitude
        Output:
            Focal plane of applied Tilt (Field)
        '''

        if amp == None:
            amp = 1

        _, _, asym_amp_field, asym_phase_field, _ = self.image_init()

        single_segment = self.segment_selector(seg = seg)

        _, tilt = np.mgrid[-np.shape(single_segment)[0]/2:np.shape(single_segment)[0]/2,
                            -np.shape(single_segment)[1]/2:np.shape(single_segment)[1]/2]

        tilt_im = single_segment * flip * tilt
        tilt_im_norm = np.where(tilt_im == 0,
                                tilt_im != 0,
                                tilt_im - np.mean(tilt_im[np.nonzero(tilt_im)])
                                )
        if self.diag:
            print('max value: ',tilt_im_norm.max())
        tilt_im_norm /= tilt_im_norm.max()

        if self.diag:
            plt.clf()
            plt.imshow(tilt_im_norm, origin='lower')
            plt.colorbar()
            plt.show()

        phase = 2. * np.pi * (amp / self.lam)

        tilt_single_seg = (tilt_im_norm * phase) #+ asym_phase_field

        if self.diag:
            # print(tilt_im_norm.max())
            plt.clf()
            plt.imshow(tilt_single_seg, origin='lower')
            plt.colorbar()
            plt.show()

        tilt_comb_focal = self.PSF_combine(asym_amp_field, asym_phase_field, mod = tilt_single_seg)

        if self.diag:
            plt.clf()
            hcipy.imshow_field(tilt_comb_focal)
            plt.colorbar()
            plt.show()


        return tilt_comb_focal, amp

def add_colorbar(mappable):
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    import matplotlib.pyplot as plt
    last_axes = plt.gca()
    ax = mappable.axes
    fig = ax.figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = fig.colorbar(mappable, cax=cax)
    plt.sca(last_axes)
    return cbar

    
def main(lam,leakage,amp_spacing,amp_range,file_name,transform,prop,plot,display,res,all,diag):

    if all == True:
        prop=True
        plot=True
        display=True
        transform=True

    RMgen = GMT_FPWFS_matrix_gen(lam,leakage,res,diag)
    segs = np.arange(1,8)

    start_time = time.time()
    if prop:
        # print('There are 126 propagations for this segement of the code.  It will take some time')
        respMatrix = []
        for seg in tqdm(segs):
            pist_pos, pist = RMgen.piston(seg)
            pist_neg, _ = RMgen.piston(seg,flip=-1)
            respMatrix.append((pist_pos - pist_neg)/(2*pist))#  Need to divide by 2 * piston amount
            tip_pos, tip = RMgen.tip(seg)
            tip_neg, _ = RMgen.tip(seg,flip=-1)
            respMatrix.append((tip_pos - tip_neg) / (2*tip))
            tilt_pos, tilt = RMgen.tilt(seg)
            tilt_neg, _ = RMgen.tilt(seg,flip=-1)
            respMatrix.append((tilt_pos - tilt_neg) / (2*tilt))
        end_time = time.time()
        print('Wall clock time for RM build: ', (end_time - start_time), 'sec')
        if plot:
            # plt.figure(figsize=20)
            for i in range(21):
                plt.subplot(7,3,i+1)
                # fig.add_subplot(3,7,i+1)
                image = hcipy.imshow_field(respMatrix[i])
                add_colorbar(image)


            if display:
                # plt.tight_layout()
                plt.show()
        if transform:


            # Plotting Singular values
            blah = hcipy.util.SVD(respMatrix)
            # print(blah.S)
            plt.clf()
            plt.plot(np.arange(0,len(blah.S),1)+1,np.log10(blah.S / np.max(blah.S)), linewidth = 4)
            plt.ylabel('Singular Value', fontsize = 25)
            plt.xlabel('Aberration Number', fontsize = 25)
            plt.title('Singular Values from Decomposition', fontsize = 30)
            plt.tick_params(labelsize = 20)
            plt.show()

            #Matrix inversion
            cont_matrix = np.transpose(hcipy.inverse_tikhonov(respMatrix))
            
            #Reference image
            ref_img = RMgen.reference_image()

            #Amplitude span resolution
            # amp_spacing = 20
            # #min/max amplitude applied to modal basis
            # amp_range = 50

            #Amplitude Vector
            test_amps = np.linspace(-amp_range,amp_range,amp_spacing)

            #List of zeros to be filled by responses
            test_res = np.zeros((amp_spacing,21))

            #Piston Response
            plt.clf()
            fig = plt.figure()
            print('Piston Response')
            for seg in tqdm(segs):

                for i,amp in enumerate(test_amps):
                    test_img, _ = RMgen.piston(seg=seg,amp=amp)
                    resid = test_img - ref_img
                    amps = cont_matrix.dot(resid)
                    test_res[i,:] = amps

                ax = fig.add_subplot(2,4,seg)
                ax.plot(test_amps,test_res)
                ax.plot(test_amps,np.transpose(test_res)[seg*3-3],'*-',label='Desired Segment Output')
                ax.plot(test_amps,test_amps,'--',label='Input Amplitude')
                plt.title('Plot from %i to %i for piston on segment %i' % (test_amps.min(),test_amps.max(),seg))
                plt.xlabel('Input amplitude (nm)')
                plt.ylabel('WFS Output Amplitude (nm)')
                plt.legend()
            plt.tight_layout()
            plt.savefig('Pist_response%i.png' % test_amps.max())
            plt.show()

            test_res = np.zeros((amp_spacing,21))

            #Tip Response
            plt.clf()
            fig = plt.figure()
            print('Tip Response')
            for seg in tqdm(segs):

                for i,amp in enumerate(test_amps):
                    test_img, _ = RMgen.tip(seg=seg,amp=amp)
                    resid = test_img - ref_img
                    amps = cont_matrix.dot(resid)
                    test_res[i,:] = amps

                # plt.clf()
                ax = fig.add_subplot(2,4,seg)
                ax.plot(test_amps,test_res)
                ax.plot(test_amps,np.transpose(test_res)[seg*3-2],'*-',label='Desired Segment Output')
                ax.plot(test_amps,test_amps,'--',label='Input Amplitude')
                plt.title('Plot from %i to %i for Tip on segment %i' % (test_amps.min(),test_amps.max(),seg))
                plt.xlabel('Input amplitude (nm)')
                plt.ylabel('WFS Output Amplitude (nm)')
                plt.legend()
            plt.tight_layout()
            plt.savefig('Tip_response%i.png' % test_amps.max())
            plt.show()

            test_res = np.zeros((amp_spacing,21))
    
            
            #Tilt Response
            plt.clf()
            fig = plt.figure()
            print('Tilt')
            for seg in tqdm(segs):

                for i,amp in enumerate(test_amps):
                    test_img, _ = RMgen.tilt(seg=seg,amp=amp)
                    resid = test_img - ref_img
                    amps = cont_matrix.dot(resid)
                    test_res[i,:] = amps
                ax = fig.add_subplot(2,4,seg)
                ax.plot(test_amps,test_res)
                ax.plot(test_amps,np.transpose(test_res)[seg*3-1],'*-',label='Desired Segment Output')
                ax.plot(test_amps,test_amps,'--',label='Input Amplitude')
                plt.title('Plot from %i to %i for Tilt on segment %i' % (test_amps.min(),test_amps.max(),seg))
                plt.xlabel('Input amplitude (nm)')
                plt.ylabel('WFS Output Amplitude (nm)')
                plt.legend()
            plt.tight_layout()
            plt.savefig('Tilt_response%i.png' % test_amps.max())
            plt.show()


    # end_time = time.time()
    # print('Wall clock time: ', end_time - start_time, 'sec')





def parse():
    '''
    Allows for command line variable manipulation rather than within the code.  Modifying variables here will propagate throughout the rest of the code.  Because that's how it should work ;)
    '''

    
    import argparse
    from str2bool import str2bool #allows for boolean parsers

    parser = argparse.ArgumentParser(description='Parser to operate Matix Generation and change of certain values',
                                    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    
    parser.add_argument('-le', '--leakage', dest = 'leakage', default=1e-1,
                        type=float, help='Intensity value of leakage term')
    parser.add_argument('-la','--lambda', dest='lam', type=float, default=532.,
                        help='Input wavelength in nm')
    parser.add_argument('-as','--amp_spacing', dest='amp_spacing', type=int, default=20,
                        help='Input amplitude spacing for response curves')
    parser.add_argument('-ar','--amp_range', dest='amp_range', type=float, default=50.,
                        help='Input +/- range of amplitudes for response curves')
    parser.add_argument('-tr','--transform',dest='transform', default=False, type=str2bool,
                        help='Take Response Matrix and apply SVD')
    parser.add_argument('-pr','--prop',dest='prop', default=True, type=str2bool,
                        help='Build Response Matrix')
    parser.add_argument('-d','--display',dest='display', default=True, type=str2bool,
                        help='Show I_diff images')
    parser.add_argument('-pl','--plot',dest='plot', default=False, type=str2bool,
                        help='Generate plots of I_diff images')  
    parser.add_argument('-a','--all',dest='all', default=True, type=str2bool,
                        help='Sets all Booleans to True or False')
    parser.add_argument('-di','--diag',dest='diag', default=False, type=str2bool,
                        help='Shows images for each def for diagnostics')                
    parser.add_argument('--file_name',dest='file_name', default='GMT_RM.fits', type=str,
                        help='Response Matrix file_path')
    parser.add_argument('-r','--res',dest='res', default='low', type=str,
                        help='Resolution of images')
    return parser

if __name__ in "__main__":

    o = parse().parse_args()
    print(o.__dict__)
    main(**o.__dict__)

