from pike import *
from alpaodm import *
import numpy as np
from matplotlib import pyplot as plt
from hcipy import *
import time

# from alpyao import AlpaoDM

#In development by Alex Tripsas

class DM_control(object):

    def __init__(self):
        self.us = 1
        self.ms = 1E3 * us
        self.s = 1E6 * us
        


class VIMBA_control(object):

    def __init__(self,binning):
        self.vimba = Vimba()
        self.system = vimba.getSystem()
        self.cam = PIKE()
        # self.binning = binning
        # self.mode = mode
        self.frame_buffer_size = 50
        self.image_buffer_size = 50
        self.num_frames = 1
        self.us = 1
        self.ms = 1E3 * us
        self.s = 1E6 * us


        #Init cam
        cam.init_camera(vimba,0)

        # Set exposure parameters
        cam.pixel_format = 'Mono16'
        cam.binning = 1
        cam.mode = 0

        # Set the frame parameters
        cam.exposure_time = 25 * us

        return

    def start(self):
        '''
        Camera and software startup
        '''
        vimba.startup()
        cam.startup()
        

    def stop_cam(self):
        '''
        Camera and software shutdown
        '''
        cam.shutdown()
        vimba.shutdown()

    # def cam_spec(self):

    #     pixel_scale = 1.22/3.9 * cam.binning
    #     N = np.array([cam.camera.Width, cam.camera.Height])
    #     cam_size = N * pixel_scale
    #     focal_grid = make_uniform_grid(N, cam_size)
    #     fpmask = circular_aperture(5)(focal_grid)

    #     cam.startup()
    #     cam.frame_counter = 0
    #     cam.prepare_frames(self.frame_buffer_size)
    #     cam.prepare_image_buffer(self.image_buffer_size)
    #     cam.num_stack = self.num_frames


def new_option_parser():

    def str2bool(v):
        if isinstance(v, bool):
            return v
        if v.lower() in ('yes', 'true', 'True', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'False', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    import argparse

    parser = argparse.ArgumentParser(description='Parse options for camera and DM settings')

    parser.add_argument('Plotting','-P', type=str2bool, default=False, 
                        help='Boolean for plotting')
    # parser.add_argument('integers', metavar='N', type=int, nargs='+',
    #             help='an integer for the accumulator')

    return parser

if __name__ == '__main__':
    blah