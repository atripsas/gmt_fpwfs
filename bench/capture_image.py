from pike import PIKE
import numpy as numpy
from hcipy import *
import time


def parse():
    
    import argparse

    parser = argparse.ArgumentParser(description='Parser to change exposure time')
    
    parser.add_argument('-t', '--exp_time',  dest = 'exp_time', 
                        type=int)
    parser.add_argument('-N',type=int, dest='N')
    return parser.parse_args(['-t','--exp_time','-N'])

if __name__ == "__main__":
    args = parse()
    exp_time = args.t
    N = args.N
    us = 1
    ms = 1e3 * us
    s = 1e6 * us

    vimba = Vimba()
    system = vimba.getSystem()
    vimba.startup()

    if system.GeVTLIsPresent:
        system.runFeatureCommand("GeVDiscoveryAllOnce")
        time.sleep(2)

    cam = PIKE()
    cam.init_camera(vimba, 0)

    cam.pixel_format = 'Mono16'
    cam.binning = 1
    cam.mode = 0

    frame_buffer_size = 50
    image_buffer_size = 50

    cam.exporsure_time = exp_time #us
    num_frames = 1

    cam.startup()
    cam.frame_counter = 0
    
    cam.prepare_frames(frame_buffer_size)
    cam.prepare_image_buffer(image_buffer_size)
    cam.num_stack = num_frames


    for i in range(0,N):
            
        img = 0
        for i in range(num_frames):
            cam.take_single_exposure()
            img += cam.get_last_image()

        date = time.localtime()
        date_list = []
        for i in range(len(date[:-3])):
            date_list.append(date[i])

        filename = 'snapshot_%i_%i_%i_%i_%i_%i.fits' % [date_list]
        write_fits(img, filename)

    cam.shutdown()
    vimba.shutdown()