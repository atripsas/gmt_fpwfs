from pike import * 
import numpy as numpy
from hcipy import *
import time
from pymba import *


def parse():
    
    import argparse

    parser = argparse.ArgumentParser(description='Parser to change exposure time')
    
    parser.add_argument('-t', type=int,help='exposure time in microseconds')
    parser.add_argument('-N',type=int, help='Number of images')
    parser.add_argument('-f',type=str, help='dark or exp')
    return parser.parse_args()

if __name__ == "__main__":
    args = parse()
    exp_time = args.t
    N = args.N
    exp_type = args.f
    us = 1
    ms = 1e3 * us
    s = 1e6 * us

    vimba = Vimba()
    system = vimba.getSystem()
    vimba.startup()

    if system.GeVTLIsPresent:
        system.runFeatureCommand("GeVDiscoveryAllOnce")
        time.sleep(2)

    cam = PIKE()
    cam.init_camera(vimba, 0)

    cam.pixel_format = 'Mono16'
    cam.binning = 1
    cam.mode = 0

    frame_buffer_size = 50
    image_buffer_size = 50

    cam.exporsure_time = exp_time #us
    num_frames = 1

    cam.startup()
    cam.frame_counter = 0
    
    cam.prepare_frames(frame_buffer_size)
    cam.prepare_image_buffer(image_buffer_size)
    cam.num_stack = num_frames


    for i in range(0,N):
        print(i)
        img = 0
        for j in range(num_frames):
            cam.take_single_exposure()
            img += cam.get_last_image()

        date = time.localtime()
        # date_list = []
        # for i in range(len(date[:-3])):
        #     date_list.append(date[i])

        filename = 'snapshot%s_%i_%i_%i_%i_%i_%i_%ius.fits' % (exp_type,i,date[0],date[1],date[2],date[3],date[4],exp_time)
        write_fits(img, filename)

    cam.shutdown()
    vimba.shutdown()