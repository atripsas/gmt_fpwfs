from pike import *
from alpaodm import *
import numpy as np
from matplotlib import pyplot as plt
from hcipy import *
import time

# from alpyao import AlpaoDM

class DM_control(object):

    def __init__(self):
        self.us = 1
        self.ms = 1E3 * us
        self.s = 1E6 * us


class VIMBA_control(object):

    def __init__(self):
        self.vimba = Vimba()
        self.system = vimba.getSystem()
        self.cam = PIKE()
        cam.init_camera(vimba,0)

        # Set exposure parameters
        cam.pixel_format = 'Mono16'
        cam.binning = 1
        cam.mode = 0

        # Set the frame parameters
	    cam.exposure_time = 25 * us

    def vim_start(self):
        vimba.startup()

    def vim_stop(self):
        vimba.shutdown()

    def start(self):
        vim_start()