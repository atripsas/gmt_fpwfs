import numpy as np 
import os
from scipy.ndimage import affine_transform
from scipy.linalg import hadamard
import json
import io

from hcipy import *

def rms(phase, aperture):
    #Calculates the RMS of the phase.
    temp_phase = np.array(phase)
    RMS = np.sqrt(np.mean((temp_phase[aperture > 0] - np.mean(temp_phase[aperture > 0])) ** 2))
    return RMS 

def orthogonalize_mode_basis(mode_basis, aperture):
    #Orthogonalizes a mode basis for an aperture.
    
    mode_basis_out = ModeBasis(mode_basis * aperture).orthogonalized
    
    #Now we normalize to 1 rms. 
    for mode in mode_basis_out:
        mode /= rms(mode, aperture)
    
    return mode_basis_out
    
def modal_decomposition(wf, basis):
    #decomposes a wavefront on a certain basis. 
    coeffs = np.dot(inverse_truncated(basis.transformation_matrix), wf.phase)
    return coeffs 

def check_directory(path):
    '''
    This function checks if the directory is already there, if not it will create a
    new directory.
    '''
    
    if not os.path.exists(path):
        os.makedirs(path)

def rebin(arr, new_shape):
    shape = (new_shape[0], arr.shape[0] // new_shape[0],
             new_shape[1], arr.shape[1] // new_shape[1])
    return arr.reshape(shape).mean(-1).mean(1)

def Fourier_shift(Rawdataframe, currentcenter):
    # Shifts frame from current center to center of image
    Lenx, Leny = Rawdataframe.shape
    grid_from  = make_pupil_grid((Lenx, Leny))

    shiftscaled = ((currentcenter[0] - Lenx / 2) / Lenx, (currentcenter[1] - Leny / 2) / Leny)
    grid_to = (grid_from.copy()).shifted(shiftscaled)

    fft = FastFourierTransform(grid_from, q=1, fov=1)
    mft = MatrixFourierTransform(grid_to, fft.output_grid)
    shifted = mft.backward(fft.forward(Field(Rawdataframe, grid_from)))

    return  shifted.real

def Fourier_scaling(Rawdataframe, scalefactor):

    # Scales the frame with scalefactor
    Lenx, Leny = Rawdataframe.shape
    grid_from  = make_pupil_grid((Lenx, Leny))
    grid_to    = (grid_from.copy()).scaled(scalefactor)

    fft = FastFourierTransform(grid_from, q=1, fov=1)
    mft = MatrixFourierTransform(grid_to, fft.output_grid)
    scaled = mft.backward(fft.forward(Field(Rawdataframe, grid_from)))

    return  scaled.real

def fourier_resample(input_data, new_shape):

    rawdataframe = input_data.copy().shaped

    # Scales the frame with scalefactor
    Lenx, Leny = rawdataframe.shape
    grid_from  = make_pupil_grid((Lenx, Leny))
    grid_to    = make_pupil_grid((new_shape[0], new_shape[1]))

    fft = FastFourierTransform(grid_from, q=1, fov=1)
    mft = MatrixFourierTransform(grid_to, fft.output_grid)
    reshaped = mft.backward(fft.forward(Field(rawdataframe.ravel(), grid_from)))

    return  reshaped.real
    
def cen_rot(im, rot, rotation_center):
    '''
    cen_rot - takes a cube of images im, and a set of rotation angles in rot,
    and translates the middle of the frame with a size dim_out to the middle of
    a new output frame with an additional rotation of rot.
    '''
    # converting rotation to radians
    a = np.radians(rot)

    # make a rotation matrix
    transform = np.array([[np.cos(a),-np.sin(a)],[np.sin(a),np.cos(a)]])[:,:]
    # calculate total offset for image output

    c_in = rotation_center#center of rotation 
    # c_out has to be pre-rotated to make offset correct

    offset = np.dot(transform, -c_in) + c_in
    offset = (offset[0], offset[1],)

    # perform the transformation
    dst = affine_transform(im, transform, offset=offset)

    return dst

def save_dict(dict, save_name):

    try:
        to_unicode = unicode
    except NameError:
        to_unicode = str

    # Write JSON file
    with io.open(save_name, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(dict,
                          indent=4, sort_keys=True,
                          separators=(',', ': '), ensure_ascii=False)
        outfile.write(to_unicode(str_))

def load_dict(load_name):

    with open(load_name) as data_file:
        dict_loaded = json.load(data_file)

    return dict_loaded

def find_recent_dictionary(directory, file_type='json'):
    '''
    This function finds the names in a give directory and returns the most recent file. 

    Assumes that the files are saved as .json 

    '''

    files = []

    #Looping over all files in the directory
    for file in os.listdir(directory):

        # only selecting files of json format 
        if file.endswith(file_type):
            files.append(os.path.join(file))

    # sorting the files 
    sorted_files = sorted(files)

    # returning the most recent file name 
    return sorted_files[-1]

def convolve(a, b):
    '''
    This function calculates the convolution c of the functions a and b by multiplying their 
    Fourier transforms.

    a = data to be convolved
    b = the kernel with which will be convolved. 
    '''

    #******************************
    #Need to check energy conservation.
    #******************************

    #Calculating the required fourier transform.
    fourier_transform = make_fourier_transform(a.grid)

    #if len(a.shape) == 1:

    #Calculating the fourier transforms of a and b. 
    a_ft = fourier_transform.forward(a)
    b_ft = fourier_transform.forward(b)

    #Calculating the Fourier transform of C, i.e. we do the convolution.
    c_ft = a_ft * b_ft 

    #Inverse fourier transforming the result and take the real component.
    c = np.real(fourier_transform.backward(c_ft))

    #lif len(a.shape) == 2:


    #else: 
    #    raise ValueError('Dont know how to handle the shape of object a!')
    
    return c

def generate_2D_Gaussian(grid, sigma):

    gaussian = Field(np.exp(- (grid.x**2 + grid.y**2) / (2 * sigma**2)) / np.sqrt(2 * np.pi) / sigma , grid)

    # normalizing to sum = 1 
    gaussian /= np.sum(gaussian) 

    return gaussian

def generate_2D_Gaussian_FT(grid_FT, sigma):

    gaussian = Field(np.exp(- sigma**2 / 2 * (grid_FT.x**2 + grid_FT.y**2)), grid_FT)

    # normalizing to sum = 1 
    gaussian /= np.sum(gaussian) 

    return gaussian

def simulate_tip_tilt_residuals(PSF, sigma):
    
    if len(PSF.shape) == 2:
        initial_power = np.sum(np.abs(PSF), axis=1)
    else:
        initial_power = np.sum(np.abs(PSF))
    
    # define grid on which the gaussian will be sampled 
    pixel_grid = make_uniform_grid(PSF.grid.shape, PSF.grid.shape)

    #Calculating the required fourier transform.
    fourier_transform = make_fourier_transform(pixel_grid)

    PSF_FT = fourier_transform.forward(PSF)

    pixel_grid_FT = PSF_FT.grid

    gaussian_kernel_FT = generate_2D_Gaussian_FT(pixel_grid_FT, sigma)

    PSF_conv_FT = PSF_FT * gaussian_kernel_FT

    PSF_conv = np.real(fourier_transform.backward(PSF_conv_FT))

    if len(PSF.shape) == 2:

        PSF_conv += 1E-16

        PSF_conv /= np.sum(np.abs(PSF_conv), axis=1)[:,np.newaxis]
        PSF_conv *= initial_power[:,np.newaxis]

    else:
         PSF_conv /= np.sum(np.abs(PSF_conv))
         PSF_conv *= initial_power

    return PSF_conv

def make_hadamard_basis(N_actuators, N_hadamard):

    grid_actuators = make_pupil_grid(N_actuators)
    grid_hadamard  = make_pupil_grid(N_hadamard)
    
    hadamard_matrix = hadamard(N_hadamard**2)

    mode_list_actuators = []
    mode_list_hadamard  = []

    for i in np.arange(N_hadamard**2):

        # this will be the part of the hadamard basis actually seen by the actuators
        mode_list_actuators.append(hadamard_matrix[i,:int(N_actuators**2)])

        # this will be the complte hadamard basis 
        mode_list_hadamard.append(hadamard_matrix[i,:])

    return ModeBasis(mode_list_actuators, grid_actuators), ModeBasis(mode_list_hadamard, grid_hadamard)

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    
    #test_flat = get_flat_field(128)
    
    #plt.figure()
    #plt.imshow(test_flat, origin='lower')
    #plt.colorbar()
    

    test_grid = make_uniform_grid([128, 128], [256, 256])

    fourier_transform = make_fourier_transform(test_grid)

    sigma = 0.5

    gaussian = generate_2D_Gaussian(test_grid, sigma)

    gaussian_FT_num = fourier_transform.forward(gaussian)

    test_grid_FT = gaussian_FT_num.grid

    gaussian_FT_ana = generate_2D_Gaussian_FT(test_grid_FT, sigma) 

    gaussian_FT_ana /= np.sum(np.abs(fourier_transform.backward(gaussian_FT_ana)))

    test_1 = fourier_transform.backward(gaussian_FT_num)
    test_2 = fourier_transform.backward(gaussian_FT_ana)

    print(np.sum(gaussian * test_grid.weights))
    print(np.sum(np.abs(gaussian_FT_num)))
    print(np.sum(np.abs(gaussian_FT_ana)))

    print('\n')
    print(np.sum(np.abs(test_1)))
    print(np.sum(np.abs(test_2)))

    plt.figure()
    imshow_field(gaussian)

    plt.figure()
    imshow_field(np.abs(gaussian_FT_num) / np.abs(gaussian_FT_num).max())

    plt.figure()
    imshow_field(gaussian_FT_ana / gaussian_FT_ana.max())

    plt.figure()
    imshow_field(gaussian_FT_ana / gaussian_FT_ana.max() - np.abs(gaussian_FT_num) / np.abs(gaussian_FT_num).max())




    print(test_grid.x)
    print(test_grid_FT.x)



    plt.show()
