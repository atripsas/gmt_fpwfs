'''
This is the second version of the forward modelling code developed for vAPPs. 
'''
import numpy as np
from scipy.optimize import minimize 
import matplotlib.pyplot as plt 

from hcipy import * 

import support_functions as sf

def make_PSF_model(amplitude, phase, propagator, focal_grid, oversampling_factor, L, V, T, focal_mask, phase_screen, wavelength, return_correction_factor=False):

    # making sure that vital arguments are floats
    L = float(L)
    V = float(V)
    T = float(T)

    # Generating the pupil electric fields for the coronagraphic and leakage PSFs.
    pupil_wf_polarization_1 = Wavefront(amplitude * np.exp(1j * phase), wavelength=wavelength)
    pupil_wf_polarization_2 = Wavefront(amplitude * np.exp(-1j * phase), wavelength=wavelength)
    pupil_wf_leakage        = Wavefront(amplitude, wavelength=wavelength)

    # Adding the phase screen. 
    pupil_wf_polarization_1.electric_field *= np.exp(1j * phase_screen)
    pupil_wf_polarization_2.electric_field *= np.exp(1j * phase_screen)
    pupil_wf_leakage.electric_field        *= np.exp(1j * phase_screen)

    # Propagating to the focal plane. 
    focal_wf_polarization_1 = propagator(pupil_wf_polarization_1)
    focal_wf_polarization_2 = propagator(pupil_wf_polarization_2)
    focal_wf_leakage        = propagator(pupil_wf_leakage)
    
    # determining the correction factors of the different PSFs 
    correction_factor_1       = np.sum(focal_wf_polarization_1.power)
    correction_factor_2       = np.sum(focal_wf_polarization_2.power)
    correction_factor_leakage = np.sum(focal_wf_leakage.power)

    PSF_polarization_1 = focal_wf_polarization_1.power
    PSF_polarization_2 = focal_wf_polarization_2.power
    PSF_leakage        = focal_wf_leakage.power
    
    # normalizing the sum of the PSFs to 1
    PSF_polarization_1 /= correction_factor_1
    PSF_polarization_2 /= correction_factor_2
    PSF_leakage        /= correction_factor_leakage

    # Generating the PSFs and adding polarization and leakage effects 
    PSF_polarization_1 *= (1 + V) / 2. * (1.- L)
    PSF_polarization_2 *= (1 - V) / 2. * (1.- L)
    PSF_leakage        *= L 

    #Combing all PSFs. 
    PSF_total_sup = PSF_polarization_1 + PSF_polarization_2 + PSF_leakage
    PSF_total     = subsample_field(PSF_total_sup, oversampling_factor, focal_grid) 

    PSF_total /= np.sum(PSF_total)

    PSF_polarization_1 = subsample_field(PSF_polarization_1, oversampling_factor, focal_grid) 
    PSF_polarization_2 = subsample_field(PSF_polarization_2, oversampling_factor, focal_grid) 
    PSF_leakage        = subsample_field(PSF_leakage, oversampling_factor, focal_grid) 

    PSF_polarization_1 /= np.sum(PSF_polarization_1)
    PSF_polarization_2 /= np.sum(PSF_polarization_2)
    PSF_leakage        /= np.sum(PSF_leakage)

    # making the focal plane mask that will change the transmission
    #focal_plane_mask             = np.ones_like(PSF_total)
    #focal_plane_mask[focal_mask] = T

    # multiplying the PSFs with this focal plane mask 
    #PSF_total *= focal_plane_mask

    if return_correction_factor:
        return PSF_total, correction_factor_1, correction_factor_2, correction_factor_leakage, PSF_polarization_1, PSF_polarization_2, PSF_leakage
    else:
        return PSF_total

def calculate_focal_plane_electric_field(amplitude, phase, propagator, phase_screen, wavelength):
    #Pupil wavefront.
    pupil_wf = Wavefront(amplitude * np.exp(1j * phase), wavelength=wavelength)
    
    #Adding the phase screen. 
    pupil_wf.electric_field *= np.exp(1j * phase_screen)
    
    #Propagating to the focal plane. 
    focal_wf = propagator(pupil_wf)
    
    return focal_wf, pupil_wf
    
def calculate_jacobian_vector(beta, args):
    '''
    Determine derivative to:
    N_p = number of photons in PSF
    N_b = background per pixel 
    mode amplitudes 

    return one jacobian vector 
    
    '''
    # counts at which element in beta we are 
    i = 0
    
    # Unpacking the parameters that might be estimated. 
    
    # checking if photon number is estimated 
    if args['estimate photon number']:
        N_ph = beta[i]
        i += 1
    else:
        N_ph = args['photon number']
    
    # checking if the background is estimated 
    if args['estimate background']:
        N_bg = beta[i]
        i += 1
    else:
        N_bg = args['background']
    
    # checking if the leakage is estimated 
    if args['estimate leakage']:
        L = beta[i]
        i += 1
    else:
        L = args['leakage']
        
    # checking if the polarization degree is estimated 
    if args['estimate polarization degree']:
        V = beta[i]
        i += 1
    else:
        V = args['polarization degree']    

    # checking if the mask transmission is estimated 
    if args['estimate mask transmission']:
        T = beta[i]
        i += 1
    else:
        T = args['mask transmission']    

    # checking if the mask transmission is estimated 
    if args['estimate alignment error']:
        alignment_error = beta[i]

        alignment_error_index = i

        i += 1
    else:
        alignment_error = args['alignment error']    

    # making sure that vital arguments are floats
    L               = float(L)
    V               = float(V)
    T               = float(T)
    alignment_error = alignment_error

    # the remaining elements of beta are modal coefficients  
    alpha = beta[i:]
    
    #Unpacking the arguments 
    data                     = args['data']
    propagator               = args['propagator']
    focal_grid               = args['focal grid']
    oversampling_factor      = args['oversampling factor']
    mode_basis               = args['mode basis']
    amplitude_vAPP           = args['amplitude vAPP']
    phase_vAPP               = args['phase vAPP']
    weight_map               = args['weight map']
    focal_mask               = args['focal plane mask']
    power                    = args['power regularization']
    wavelength               = args['wavelength (m)']
    include_alignment_errors = args['include alignment errors']
    
    #The vector containing the jacobian. 
    jacobian_vector = np.zeros_like(beta)

    #We create the phase screen. 
    phase_screen = np.sum(alpha[:, np.newaxis] * mode_basis, axis=0)

    #First we calculate the model. 
    model, correction_factor_1, correction_factor_2, correction_factor_leakage, PSF_1, PSF_2, PSF_L = make_PSF_model(amplitude_vAPP, phase_vAPP, propagator, focal_grid, oversampling_factor, L, V, T, focal_mask, phase_screen, wavelength, return_correction_factor=True)

    # making the focal plane mask that will change the transmission
    focal_plane_mask             = np.ones_like(focal_grid.x)
    focal_plane_mask[focal_mask] = T

    # determining the effect of noise 
    if N_bg > 0.:

        # assuming that the background noise is shot noise, but constant over the detector 
        sigma_bg = np.sqrt(N_bg)

        # the shot noise in the PSF, determined by removing the background 
        select_pos = (data - N_bg > 0)
        sigma_ph = np.ones_like(data) * 1E-4
        sigma_ph[select_pos] = np.sqrt(data[select_pos] - N_bg)

        # total noise contribution 
        sigma = np.sqrt(sigma_bg**2 + sigma_ph**2)
    else:
        # now we only assume photon noise 
        sigma = 1#np.sqrt(data)
    
    total_model = N_ph * model + N_bg

    if include_alignment_errors: 
        # here we convolve with Gaussian to simulate alignment errors. 
        total_model = sf.simulate_tip_tilt_residuals(total_model, alignment_error)

    #Then we calculate the first term. 
    term_1 = -weight_map * (data - total_model) / (sigma)**2

    # counts at which element in the Jacobian vector we are 
    j = 0

    # checking if derivative to photon number is required
    if args['estimate photon number']:
        temp_photon_number = model

        if include_alignment_errors: 
            # here we convolve with Gaussian to simulate alignment errors. 
            temp_photon_number = sf.simulate_tip_tilt_residuals(temp_photon_number, alignment_error)

        jacobian_vector[j] = np.sum(temp_photon_number * term_1)
        j += 1 
    
    # checking if derivative to background is required
    if args['estimate background']:
        jacobian_vector[j] = np.sum(term_1)
        j += 1 
        
    # checking if derivative to leakage is required
    if args['estimate leakage']:
        temp_leakage =  N_ph * focal_plane_mask * (-(1 + V) / 2 * PSF_1 + (V - 1) / 2 * PSF_2 + PSF_L)

        if include_alignment_errors: 
            # here we convolve with Gaussian to simulate alignment errors. 
            temp_leakage = sf.simulate_tip_tilt_residuals(temp_leakage, alignment_error)

        jacobian_vector[j] = np.sum(term_1 * temp_leakage)
        j += 1 

    # checking if derivative to polarization degree is required
    if args['estimate polarization degree']:
        temp_pol_degree =  N_ph * focal_plane_mask * ((1 - L) / 2 * PSF_1 + (L - 1) / 2 * PSF_2) 

        if include_alignment_errors: 
            # here we convolve with Gaussian to simulate alignment errors. 
            temp_pol_degree = sf.simulate_tip_tilt_residuals(temp_pol_degree, alignment_error)

        jacobian_vector[j] = np.sum(term_1 * temp_pol_degree) 
        j += 1 
        
    # checking if derivative to mask transmission degree is required
    if args['estimate mask transmission']:
        temp_mask_transmission =  N_ph * ((1 + V) / 2 * (1 - L) * PSF_1 + (1 - V) / 2 * (1 - L) * PSF_2 + L * PSF_L)

        if include_alignment_errors: 
            # here we convolve with Gaussian to simulate alignment errors. 
            temp_mask_transmission = sf.simulate_tip_tilt_residuals(temp_mask_transmission, alignment_error)

        jacobian_vector[j] = np.sum(term_1 * temp_mask_transmission)
        j += 1 

    # checking if derivative to mask transmission degree is required
    if args['estimate alignment error']:
        
        sigma = 1E-8

        beta_plus = beta.copy()
        beta_plus[alignment_error_index] += sigma

        beta_minus = beta.copy()
        beta_minus[alignment_error_index] -= sigma

        jacobian_vector[j] = np.sum((merit_function(beta_plus, args) - merit_function(beta_minus, args)) / (2 * sigma))

        j += 1 

    #Calculating the electric fields of the three PSFs.
    focal_wf_1, pupil_wf_1 = calculate_focal_plane_electric_field(amplitude_vAPP, phase_vAPP, propagator, phase_screen, wavelength)
    focal_wf_2, pupil_wf_2 = calculate_focal_plane_electric_field(amplitude_vAPP, -phase_vAPP, propagator, phase_screen, wavelength)
    focal_wf_3, pupil_wf_3 = calculate_focal_plane_electric_field(amplitude_vAPP, 0, propagator, phase_screen, wavelength)

    #Creating one vector of all the modes times the pupil plane electric fields.
    pupil_wf_vector_1 = Wavefront(mode_basis * pupil_wf_1.electric_field[np.newaxis,:], wavelength=wavelength)
    pupil_wf_vector_2 = Wavefront(mode_basis * pupil_wf_2.electric_field[np.newaxis,:], wavelength=wavelength) 
    pupil_wf_vector_3 = Wavefront(mode_basis * pupil_wf_3.electric_field[np.newaxis,:], wavelength=wavelength)

    #Propagating to the focal plane.
    focal_wf_vector_1 = propagator(pupil_wf_vector_1)
    focal_wf_vector_2 = propagator(pupil_wf_vector_2)
    focal_wf_vector_3 = propagator(pupil_wf_vector_3)

    #Calculating the derivate terms for the three PSFs.
    derivative_term_1 = 1j * focal_wf_vector_1.electric_field * focal_wf_1.electric_field[np.newaxis,:].conj() - 1j * focal_wf_1.electric_field[np.newaxis,:] * focal_wf_vector_1.electric_field.conj()
    derivative_term_2 = 1j * focal_wf_vector_2.electric_field * focal_wf_2.electric_field[np.newaxis,:].conj() - 1j * focal_wf_2.electric_field[np.newaxis,:] * focal_wf_vector_2.electric_field.conj()
    derivative_term_3 = 1j * focal_wf_vector_3.electric_field * focal_wf_3.electric_field[np.newaxis,:].conj() - 1j * focal_wf_3.electric_field[np.newaxis,:] * focal_wf_vector_3.electric_field.conj()

    #Multiplying with the grid weights to get the power.        
    derivative_term_1 *= focal_wf_1.grid.weights[np.newaxis,:]
    derivative_term_2 *= focal_wf_1.grid.weights[np.newaxis,:]
    derivative_term_3 *= focal_wf_1.grid.weights[np.newaxis,:]
    
    #Correcting with the total normalization.
    derivative_term_1 /= correction_factor_1
    derivative_term_2 /= correction_factor_2
    derivative_term_3 /= correction_factor_leakage
    
    # correcting for leakage and polarization effects 
    derivative_term_1 *= (1 + V) / 2 * (1 - L)
    derivative_term_2 *= (1 - V) / 2 * (1 - L)
    derivative_term_3 *= L 

    # downsampling the derivative
    temp_derivative_sup = (derivative_term_1 + derivative_term_2 + derivative_term_3)
    temp_derivative     = subsample_field(temp_derivative_sup, oversampling_factor, focal_grid) 

    # simple derivative for the regularization
    regularization_derivative = alpha / ((np.arange(len(alpha)) + 1)**power)

    temp_term = np.real(N_ph *  focal_plane_mask[np.newaxis,:] * temp_derivative)

    if include_alignment_errors: 
        # here we convolve with Gaussian to simulate alignment errors. 
        temp_term = sf.simulate_tip_tilt_residuals(temp_term, alignment_error)

    total_derivative = np.real(term_1[np.newaxis,:] * temp_term)

    #Adding all the derivative terms to get the complete term. 
    jacobian_vector[j:] = np.sum(total_derivative, axis=1) + regularization_derivative

    return jacobian_vector

def merit_function(beta, args): 
    '''
    beta = list / np.array with parameters to be estimated. 
    args = dictionary consisting of: 
    
    'data' = 
    'propagator' = 
    'mode basis' = 
    'amplitude vAPP' = 
    'phase vAPP' = 
    'leakage' = 
    'polarization degree' =
    'focal plane mask shape' = 
    'weight map' = 

    '''
    # counts at which element in beta we are 
    i = 0
    
    # Unpacking the parameters that might be estimated. 
    
    # checking if photon number is estimated 
    if args['estimate photon number']:
        N_ph = beta[i]
        i += 1
    else:
        N_ph = args['photon number']
        
    # checking if the background is estimated 
    if args['estimate background']:
        N_bg = np.abs(beta[i])
        i += 1
    else:
        N_bg = args['background']
    
    # checking if the leakage is estimated 
    if args['estimate leakage']:
        L = beta[i]
        i += 1
    else:
        L = args['leakage']
        
    # checking if the polarization degree is estimated 
    if args['estimate polarization degree']:
        V = beta[i]
        i += 1
    else:
        V = args['polarization degree']    

    # checking if the mask transmission is estimated 
    if args['estimate mask transmission']:
        T = beta[i]
        i += 1
    else:
        T = args['mask transmission']  

    # checking if the mask transmission is estimated 
    if args['estimate alignment error']:
        alignment_error = beta[i]
        i += 1
    else:
        alignment_error = args['alignment error']    

    # making sure that vital arguments are floats
    L               = float(L)
    V               = float(V)
    T               = float(T)
    alignment_error = alignment_error

    # the remaining elements of beta are modal coefficients  
    alpha = beta[i:]

    #Unpacking the arguments 
    data                     = args['data']
    propagator               = args['propagator']
    focal_grid               = args['focal grid']
    oversampling_factor      = args['oversampling factor']
    mode_basis               = args['mode basis']
    amplitude_vAPP           = args['amplitude vAPP']
    phase_vAPP               = args['phase vAPP']
    weight_map               = args['weight map']
    focal_mask               = args['focal plane mask']
    power                    = args['power regularization']
    wavelength               = args['wavelength (m)']
    include_alignment_errors = args['include alignment errors']

    #We create the phase screen. 
    phase_screen = np.sum(alpha[:, np.newaxis] * mode_basis, axis=0)
    
    #We create the PSF model that include the phase screen.
    model = make_PSF_model(amplitude_vAPP, phase_vAPP, propagator, focal_grid, oversampling_factor, L, V, T, focal_mask, phase_screen, wavelength)

    # determining the effect of noise 
    if N_bg > 0.:
        # assuming that the background noise is shot noise, but constant over the detector 
        sigma_bg = np.sqrt(N_bg)

        # the shot noise in the PSF, determined by removing the background 
        select_pos = (data - N_bg > 0)
        sigma_ph = np.ones_like(data) * 1E-4
        sigma_ph[select_pos] = np.sqrt(data[select_pos] - N_bg)

        # total noise contribution 
        sigma = np.sqrt(sigma_bg**2 + sigma_ph**2)
    else:
        # now we only assume photon noise 
        sigma = 1#np.sqrt(data)

    # the regularization term 
    regularization = 0.5 * np.sum(alpha**2 / ((np.arange(len(alpha)) + 1 )**power))
    
    # adding the photon number and the background to the model.
    complete_model = N_ph * model + N_bg

    if include_alignment_errors: 

        # here we convolve with Gaussian to simulate alignment errors. 
        complete_model = sf.simulate_tip_tilt_residuals(complete_model, alignment_error)
    """
    plt.figure()
    imshow_field(np.log10(complete_model))
    plt.colorbar()

    plt.figure()
    imshow_field(np.log10(data))
    plt.colorbar()

    plt.show()
    """
    #We calculate the merit function.
    merit = np.sum(weight_map * (data - complete_model)**2 / (2 * (sigma)**2)) + regularization
    
    return merit 

def estimate_aberration_coefficients(args, algorithm='L-BFGS-B', analytical_jacobian=True):
    '''
    This function uses forward modelling to 

    modes in mode_basis must normalized to an RMS of 1. 

    speed increases when the pupils are not too large. 
    
    
    
    args = dictionary that carries the following keywords: 
    
    general keywords 
    'data'                      = the data used for the estimation
    'propagator'                = operator from pupil plane to focal plane 
                                  sampled on correct pupil and focal grids
    'mode basis'                = array of the mode basis 
    'amplitude vAPP'            = array with vAPP amplitude 
    'phase vAPP'                = array with vAPP phase 
    
    'weight map'                = can be used to give extra weight to certain
                                  areas in the focal plane, can also be None 
    'focal plane mask'          = mask that shows where the ND focal plane
                                  mask is applied [boolean]
    
    keywords connected to the start vector 
    'estimate start vector' = 'advanced', 'simple', 'given' 
    'start vector' = if above is 'given', add the start vector here 
    
                     start vector should look like this: 
                     (remove all elements that are not estimated)
                     np.array([photon number estimate,
                               background estimate,
                               leakage estimate,
                               polarization estimate,
                               transmission estimate,
                               coefficient estimates,])
    
    these keywords are used for advanced start vector estimation 
    'select background'         = selection of the focal plane used to 
                                  get an estimate of the background 
    'select wavefront sensing'  = selection of the focal plane used to 
                                  get an first estimate of the wavefront
    
    
    
    these keywords decide which parameters are estimated or not 
    aberration coefficients will always be estimated 
    'estimate photon number'       = [boolean]
    'estimate background'          = [boolean]
    'estimate leakage'             = [boolean]
    'estimate polarization degree' = [boolean]
    'estimate mask transmission'   = [boolean]
    
    all the parameters that are not estimated need to be given as: 
    'photon number'       = [int]
    'background'          = [float]
    'leakage'             = [0,1]
    'polarization degree' = [0,1]
    'mask transmission'   = [0,1]
    
    Example dictionary 
    args = {'data'                        : ...,
            'propagator'                  : ...,
            'mode basis'                  : ...,
            'amplitude vAPP'              : amplitude,
            'phase vAPP'                  : phase,
            'weight map'                  : None,
            'focal plane mask'            : None,
            'estimate start vector'       : 'given',
            'start vector'                : ...,
            'select background'           : False,
            'select wavefront sensing'    : false,
            'estimate photon number'      : False,
            'estimate background'         : False,
            'estimate leakage'            : False,
            'estimate polarization degree': False,
            'estimate mask transmission'  : False,
            'photon number'               : 1,
            'background'                  : 0,
            'leakage'                     : 0,
            'polarization degree'         : 0,
            'mask transmission'           : 1,
            'minimization tolerance'      : 1E-4,
            }
    
    '''
    
    if args['estimate start vector'] == 'advanced':
        
        raise ValueError('Not yet implemented')
    
        print('\nCalculating advanced estimation of the start vector.')

        start_position = calculate_start_position(args)
        
    elif args['estimate start vector'] == 'simple':
        
        raise ValueError('Not yet implemented')
        
        print('\nCalculating simple estimation of the start vector.')
        
        start_position = np.zeros(args['mode basis'].shape[0] + 2)
        
        start_position[0] = np.sum(args['data'])
        start_position[1] = np.mean(args['data']) #this will be an overestimation as the PSF is included as well 
        
    elif args['estimate start vector'] == 'given':
        
        print('\nUsing provided start vector.')
        
        start_position = args['start vector']
    else:
        raise ValueError('Unclear how to determine start vector!')
    
    # tolerance for minimization 
    tolerance = args['minimization tolerance']

    #print('\nEstimating'

    print('\nstarting the estimation.')

    if analytical_jacobian:
        jacobian = calculate_jacobian_vector
    else:
        jacobian = None
    
    result = minimize(merit_function, 
                      start_position,
                      args=(args), 
                      method=algorithm,                             
                      jac=jacobian,
                      tol=None, 
                      callback=None, 
                      options={'disp': True,
                               'ftol': tolerance, 
                               'gtol': tolerance,
                               #'return_all': False, 
                               'maxiter': 200,
                      })
    
    '''
    result = least_squares(merit_function, 
                      start_position,
                      args=(args,),
                      jac=calculate_jacobian_vector,
                      xtol=tolerance,
                      gtol=tolerance,
                      verbose=2, 
                      )
    '''

    return result.x, result.fun

if __name__ == '__main__':
    import generate_data as gd
    ####################################################################
    #FORWARD MODELLING TEST CODE
    ####################################################################
    
    # Directory and file names for SCExAO vAPP Design that we use for 
    # testing the code 
    path_fits_file      = 'reference/'
    file_vAPP_amplitude = 'SCExAO_vAPP_amplitude_resampled.fits'
    file_vAPP_phase     = 'SCExAO_vAPP_phase_resampled.fits'

    # Loading the phase and amplitude of the vAPP. 
    amplitude_vAPP  = read_fits(path_fits_file + file_vAPP_amplitude)
    phase_vAPP      = read_fits(path_fits_file + file_vAPP_phase)

    # the vAPP mask is undersized by 1.5%
    shrink_factor = 0.015

    wavelength = 1550.E-9 # meter

    # diameter telescope 
    diameter   = 7.8 * (1 - shrink_factor) # meter 

    # lambda / diameter 
    ld = wavelength / diameter # radians
    ld = np.degrees(ld) * 3600. * 1000 # milli arcsec

    # number of pixels along on axis in the focal and pupil planes
    Npup = 256 
    Nfoc = 128

    print('setting up grids, propagator, etc.')
    # building the grids 
    pupil_grid = make_pupil_grid(Npup, diameter)

    rad_pix = np.radians(15.6 / 1000 / 3600)
    
    focal_grid = make_uniform_grid([Nfoc, Nfoc], [Nfoc*rad_pix, Nfoc*rad_pix])

    oversampling_factor_est = 2
    focal_grid_sup_est      = make_supersampled_grid(focal_grid, oversampling_factor_est)
    propagator_est          = FraunhoferPropagator(pupil_grid, focal_grid_sup_est)

    oversampling_factor_data = 2
    focal_grid_sup_data      = make_supersampled_grid(focal_grid, oversampling_factor_data)
    propagator_data          = FraunhoferPropagator(pupil_grid, focal_grid_sup_data)

    # Turning the files into fields.
    amplitude_vAPP = Field(amplitude_vAPP.ravel(), pupil_grid)
    phase_vAPP     = Field(phase_vAPP.ravel(), pupil_grid) 
    
    ####################################################################
    #PROPERTIES OF THE VAPP
    ####################################################################  
    
    # add focal plane mask 
    def ND_mask(grid):
        mask = grid.rotated(np.radians(-25 - 69)).x < 0.1
        mask += grid.rotated(np.radians(-70 - 69 )).x > 0.1
         
        mask2 = grid.rotated(np.radians(-73 + 180 - 69)).x > -0.1
        mask2 += grid.rotated(np.radians(-28 + 180 - 69)).x < -0.1
         
        mask3 = 1 - circular_aperture(8)(grid)
        masktot = mask * mask2 * mask3
	 
        mask_out = Field(np.zeros_like(masktot, dtype=bool), grid)
        mask_out[masktot == False] = True
     
        return mask_out
    
    # properties of the focal plane mask 
    mask = ND_mask(focal_grid)
    T_mask = 1
     
    # making the focal plane mask 
    fp_mask = Field(np.ones_like(focal_grid.x), focal_grid)
    fp_mask[mask] = T_mask
    
    weight_map = np.ones_like(mask, dtype=int)

    leakage    = 0.05
    degree_pol = 0.0
    Nphot      = 1E6

    ####################################################################
    # WE CREATE TEST DATA 
    ####################################################################
    rms_wfe = 0.3 # rad 

    #Making phase aberrations.
    #phase_aberration = gd.make_PSD_phase_screen(pupil_grid, amplitude_vAPP, rms_wfe, power=-3, remove_tip_tilt=True)
    phase_aberration = gd.make_zernike_phase_screen(pupil_grid, amplitude_vAPP, 30, rms_wfe, wavelength, diameter, remove_tip_tilt=True)
    
    #phase_aberration.electric_field *= np.exp(2j * np.pi * (pupil_grid.x * 5 + pupil_grid.y * 5))
    
    additional_options = {'add aberration'         : True,
                          'aberration'             : phase_aberration,
                          'photon number'          : Nphot,
                          'add photon noise'       : True,
                          'background'             : 10,
                          'read noise'             : 0,
                          'flat field error'       : 0.,
                          'saturation level'       : None,
                          'save data'              : False,
                          'save path'              : 'test/',
                          'leakage'                : leakage,
                          'polarization degree'    : degree_pol,
                          'add focal plane mask'   : False, 
                          'focal plane mask'       : fp_mask, 
                          'rms wfe'                : rms_wfe,
                          'vAPP files path'        : '/',
                          'vAPP amplitude file'    : file_vAPP_amplitude,
                          'vAPP phase file'        : file_vAPP_phase,
                          'wavelength'             : wavelength,
                          'oversampling factor'    : oversampling_factor_data,
                          'focal grid'             : focal_grid,
                          }

    test_data = gd.make_artifical_data(amplitude_vAPP, phase_vAPP, propagator_data, additional_options)

    test_data = sf.simulate_tip_tilt_residuals(test_data, 0.5)

    '''
    plt.figure()
    imshow_field(np.log10(np.abs(test_data)))
    plt.colorbar()

    plt.show()
    '''
    ####################################################################
    # WE ESTIMATE THE DATA
    ####################################################################
    Nmodes = 30 
    
    # making the mode basis  
    mode_basis = make_zernike_basis(Nmodes, diameter, pupil_grid, 4)#make_disk_harmonic_basis(pupil_grid, Nmodes) #
    
    # orthogonalize the mode basis 
    #mode_basis = sf.orthogonalize_mode_basis(mode_basis, amplitude_vAPP)
    
    mode_basis_array = np.array(mode_basis)
    
    start_vector = np.zeros(Nmodes + 5)
    start_vector[0] = Nphot
    start_vector[1] = 10
    start_vector[2] = leakage
    start_vector[3] = degree_pol
    start_vector[4] = 0.3
    
    args = {'data'                        : test_data,
            'propagator'                  : propagator_est,
            'focal grid'                  : focal_grid,
            'oversampling factor'         : oversampling_factor_est,
            'mode basis'                  : mode_basis_array,
            'amplitude vAPP'              : amplitude_vAPP,
            'phase vAPP'                  : phase_vAPP,
            'weight map'                  : weight_map,
            'focal plane mask'            : mask,
            'estimate start vector'       : 'given',
            'start vector'                : start_vector,
            'select background'           : False,
            'select wavefront sensing'    : False,
            'estimate photon number'      : True,
            'estimate background'         : True,
            'estimate leakage'            : True,
            'estimate polarization degree': True,
            'estimate mask transmission'  : False,
            'estimate alignment error'    : True, 
            'photon number'               : Nphot,
            'background'                  : 0,
            'leakage'                     : leakage,
            'polarization degree'         : 0.0,
            'mask transmission'           : 1,
            'minimization tolerance'      : 1E-4,
            'power regularization'        : -3., 
            'wavelength (m)'              : wavelength,
            'include alignment errors'    : True,
            'alignment error'             : 0.3,
            }
    
    estimated_parameters, _ = estimate_aberration_coefficients(args, analytical_jacobian=True)

    print('\nbest merit = ', np.sum(np.abs(np.sqrt(test_data[test_data > 0]))**2 / (2 * np.sqrt(test_data[test_data > 0])**2)))

    print(estimated_parameters)

    estimated_coefs = estimated_parameters[5:]
    
    est_align_error = estimated_parameters[4]

    #print('\nEstimated parameters: \n', estimated_parameters)
    
    print('\nEstimated modal coeffs: \n', estimated_coefs)

    # projecting the phase screen on the mode basis:
    projected_coefs = sf.modal_decomposition(phase_aberration, mode_basis)
    
    print('\nProjected coeffs: \n', projected_coefs)
    
    print('\nrelative error: \n', np.abs(projected_coefs - estimated_coefs) / np.abs(projected_coefs))
    
    introduced_phase = Field(phase_aberration.phase, pupil_grid)
    estimated_phase  = Field(np.sum(mode_basis_array * estimated_coefs[:,np.newaxis], axis=0), pupil_grid) * (amplitude_vAPP > 1E-8)
    residual_phase   = estimated_phase - introduced_phase

    vmax = np.max(np.abs(introduced_phase))

    estimated_phase_wf = Wavefront(amplitude_vAPP * np.exp(1j* estimated_phase), wavelength=wavelength)

    additional_options = {'add aberration'         : True,
                          'aberration'             : estimated_phase_wf,
                          'photon number'          : Nphot,
                          'add photon noise'       : False,
                          'background'             : 0,
                          'read noise'             : 0,
                          'flat field error'       : 0.,
                          'saturation level'       : None,
                          'save data'              : False,
                          'save path'              : 'test/',
                          'leakage'                : leakage,
                          'polarization degree'    : degree_pol,
                          'add focal plane mask'   : False, 
                          'focal plane mask'       : fp_mask, 
                          'rms wfe'                : rms_wfe,
                          'vAPP files path'        : '/',
                          'vAPP amplitude file'    : file_vAPP_amplitude,
                          'vAPP phase file'        : file_vAPP_phase,
                          'wavelength'             : wavelength,
                          'oversampling factor'    : oversampling_factor_est,
                          'focal grid'             : focal_grid,
                          }

    est_data = gd.make_artifical_data(amplitude_vAPP, phase_vAPP, propagator_est, additional_options)

    est_data = sf.simulate_tip_tilt_residuals(est_data, est_align_error)

    plt.figure()
    
    plt.subplot(1,2,1)
    imshow_field(np.log10(np.abs(test_data) / test_data.max()), vmin=-5)
    plt.colorbar()
    
    plt.subplot(1,2,2)
    imshow_field(test_data)
    plt.colorbar()


    plt.title('data')

    plt.figure()
    
    plt.subplot(1,2,1)
    imshow_field(np.log10(est_data / est_data.max()), vmin=-5)
    plt.colorbar()
    
    plt.subplot(1,2,2)
    imshow_field(est_data)
    plt.colorbar()

    plt.title('est data')


    plt.figure()
    
    imshow_field(test_data - est_data)
    plt.colorbar()

    plt.title('residuals')

    plt.figure()
    
    imshow_field((test_data - est_data) / (np.abs(test_data) + 1E-4), cmap='bwr', vmin=-1, vmax=1)
    plt.colorbar()

    plt.title('normalized residuals')


    plt.figure()

    plt.subplot(1,3,1)
    imshow_field(introduced_phase, cmap='bwr', vmin=-vmax, vmax=vmax)
    plt.colorbar()

    plt.title('introduced phase')

    plt.subplot(1,3,2)
    imshow_field(estimated_phase, cmap='bwr', vmin=-vmax, vmax=vmax)
    plt.colorbar()

    plt.title('estimated phase')

    plt.subplot(1,3,3)
    imshow_field(residual_phase, cmap='bwr', vmin=-vmax, vmax=vmax)
    plt.colorbar()

    plt.title('residual phase = estimated - introduced')

    plt.show()
    
    


    
