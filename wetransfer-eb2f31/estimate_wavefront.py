# setting the max number of processors that can be used. 
import os
import numpy as np
import matplotlib.pyplot as plt 
import time
import datetime
from scipy import ndimage
from skimage.feature import register_translation
from scipy.ndimage import shift

from hcipy import * 

import forward_modelling as fm
import support_functions as sf 

def imgarr_to_field(im):
    '''
    Code segment by Alex Tripsas
    Takes image array and outputs field and grid
    '''
    grid = make_pupil_grid(im.shape[0], diameter = 1)
    im_flat = im.flatten()
    flat_field = np.ones(len(im_flat))
    field = Field(flat_field, grid)
    field *= im_flat
    return field, grid


def measure_polarization_degree(data,x_1,x_2,y_1,y_2,radius):
    
    # the used grid
    grid = data.grid 

    # coordinates of the two PSFs 
    # x_1 =  #-9.66E-5#-1.7E-4 # radians
    # y_1 = 1.76E-3#1.4E-3 # radians
    # x_2 = 6.37E-5#-x_1
    # y_2 = 1.77E-3#-y_1

    # radius of the circle that will be used to measure I
    # radius = 4E-4 # radians

    # selecting the areas that we wil use to measure the flux in the PSFs 
    select_PSF_1 = (grid.x - x_1)**2 + (grid.y - y_1)**2 <= radius**2
    select_PSF_2 = (grid.x - x_2)**2 + (grid.y - y_2)**2 <= radius**2

    # measuring the total flux in the two PSFs
    I_PSF_1 = np.sum(data[select_PSF_1])
    I_PSF_2 = np.sum(data[select_PSF_2])

    # calculating the degree of polarization 
    pol_degree = (I_PSF_1 - I_PSF_2) / (I_PSF_1 + I_PSF_2)

    return float(pol_degree) 

def measure_leakage(data,x_1,x_2,x_3,y_1,y_2,y_3,radius):

    # the used grid
    grid = data.grid 
    
    # # coordinates of the two PSFs 
    # x_1 = -8.09e-05     #-9.66E-5#-1.7E-4 # radians   #-8.09e-05 0.00173
    # y_1 = 0.00173 #1.76E-3#1.4E-3 # radians 
    # x_2 = 8.28e-05  #6.37E-5#-x_1 #   8.28e-05 -0.00182
    # y_2 = -0.00182  #1.77E-3#-y_1 # 
    # x_3 = 9.95e-06 #-1.13E-5  #Centroid   9.95e-06 -4.92e-05
    # y_3 = -4.92e-05 #-3.12E-6  # 

    # # radius of the circle that will be used to measure I
    # radius = 4E-4 # radians

    # selecting the areas that we wil use to measure the flux in the PSFs 
    select_PSF_1   = (grid.x - x_1)**2 + (grid.y - y_1)**2 <= radius**2
    select_PSF_2   = (grid.x - x_2)**2 + (grid.y - y_2)**2 <= radius**2
    select_leakage = (grid.x - x_3)**2 + (grid.y - y_3)**2 <= radius**2

    # measuring the total flux in the two PSFs
    I_PSF_1   = np.sum(data[select_PSF_1]) 
    I_PSF_2   = np.sum(data[select_PSF_2]) 
    I_leakage = np.sum(data[select_leakage])

    # estimation of the total flux received by system
    total_flux = I_PSF_1 + I_PSF_2 + I_leakage

    # calculating my definition of the leakage from that
    L = (total_flux - I_PSF_1 - I_PSF_2 ) / total_flux # better SNR than I_leakage/total_flux

    return L 

def measure_background(data):

    # the used grid
    grid = data.grid 

    # the zones that we use to estimate the background
    select_zone_1 = (grid.x <= -2.E-3) * (grid.y <= -2.E-3)
    select_zone_2 = (grid.x >= 2.E-3) * (grid.y >= 2.E-3)

    # adding the zones
    select_zones = select_zone_1 + select_zone_2
    
    # estimating the background
    bg_est = np.std(data[select_zones])

    return float(bg_est)
    
def ND_mask(Npix):

    grid = make_pupil_grid(Npix)

    # y = -1.025 * x + 0.121

    select_leakage = (grid.x + 0.0005)**2 + (grid.y - 0.008) **2 < 0.066**2

    select_lower = (grid.y < 0) * (grid.y + 1.025 * grid.x - 0.0121 + 0.007 > 0)
    select_upper = (grid.y > 0) * (grid.y + 1.025 * grid.x - 0.0121 + 0.007 < 0)

    total_select = select_leakage + select_lower + select_upper

    total_select = np.reshape(total_select, (Npix, Npix))

    total_select = total_select[:,::-1]

    total_select = Field(total_select.ravel(), grid)

    return total_select

def estimate_wavefront(data, model_parameters, Nmodes=30, used_mode_basis='zernike', tolerance=1E-4, use_pupil_amplitude=False, include_alignment_errors=False, 
                        oversampling_factor=1, est_parameters=np.array([False,False,False,False,False]), use_analytical_jacobian=True):
    '''
    This code estimates the wavefront from a APvAPP focal-plane image. 

    parameters:
    data 

    Nmodes

    used_mode_basis 

    use_pupil_amplitude 

    include_alignment_errors
    oversampling_factor
    est_parameters
    use_analytical_jacobian
    
    '''
    # extracting the model parameters 
    mas_pix        = model_parameters['mas / pix']
    rotation_angle = model_parameters['rotation angle (deg)']
    wavelength     = model_parameters['wavelength (meter)']
    diameter       = model_parameters['diameter (meter)']
    Npup           = model_parameters['Npix pupil plane']
    Nfoc           = model_parameters['Npix focal plane']
    amplitude_file = model_parameters['Amplitude file']
    phase_file     = model_parameters['Phase file']

    if use_pupil_amplitude: 
        file_pupil_amplitude = model_parameters['Measured pupil amplitude file']
        
    #also needs to load position of PSFs + how to estimate background

    # lambda / diameter 
    ld = wavelength / diameter # radians
    ld = np.degrees(ld) * 3600. * 1000 # milli arcsec

    # transmission of the ND mask 
    T = 1

    #-------------------------------------------------------------------
    # Files and paths 
    #-------------------------------------------------------------------

    # current date as save directory
    current_date = datetime.datetime.today().strftime('%Y-%m-%d')
    
    # current time for file saving 
    current_time = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    
    save_path    = 'results/' + current_date + '/' + current_time + '/'

    # checking if directories are already there
    sf.check_directory(save_path)

    print('mas / pix = ', mas_pix)
    print('rotation_angle (deg) = ', rotation_angle)

    if include_alignment_errors:

        alignment_error = 0
        
        print('\nAlignment error = ', alignment_error)

    else:
        alignment_error = None

    #-----------------------------------------------------------------
    # Setting up Field, mode basis, etc 
    #-----------------------------------------------------------------
    print('setting up grids, propagator, etc.')
    # building the grids 
    pupil_grid = make_pupil_grid(Npup, diameter)

    rad_pix = np.radians(mas_pix / 1000 / 3600)
    
    focal_grid = make_uniform_grid([Nfoc, Nfoc], [Nfoc*rad_pix, Nfoc*rad_pix])

    # grid associated to this oversampling
    focal_grid_sup = make_supersampled_grid(focal_grid, oversampling_factor)
    
    # making the propagator 
    propagator = FraunhoferPropagator(pupil_grid, focal_grid_sup)

    #-------------------------------------------------------------------
    # making the weight map and the ND mask
    #-------------------------------------------------------------------

    # loading the ND mask 
    mask = np.ones(Nfoc*Nfoc, dtype=bool)#ND_mask(Nfoc)

    # setting the weight map 
    weight_map = np.ones(Nfoc*Nfoc)

    #weight_map[mask] = 0

    #-----------------------------------------------------------------
    # loading relevant files
    #-----------------------------------------------------------------
    print('loading files')
    # loading the amplitude and phase 
    amplitude = read_fits(amplitude_file)
    phase     = read_fits(phase_file)
     
    # rotating them 
    amplitude = sf.cen_rot(amplitude, rotation_angle, np.array([Npup/2, Npup/2]))
    phase     = sf.cen_rot(phase, rotation_angle, np.array([Npup/2, Npup/2]))

    # converting them to Fields 
    amplitude = Field(amplitude.ravel(), pupil_grid)
    phase     = Field(phase.ravel(), pupil_grid)

    if use_pupil_amplitude:

        pupil_amplitude = read_fits(file_pupil_amplitude)
        pupil_amplitude = Field(pupil_amplitude.ravel(), pupil_grid)
        
        # first we find the offset          
        offset = register_translation(amplitude.shaped, pupil_amplitude.shaped, upsample_factor=10)[0]

        # then we shift 
        pupil_amplitude = Field(shift(pupil_amplitude.shaped, offset, order=5).ravel(), pupil_grid)

        # normalize it 
        pupil_amplitude /= np.median(pupil_amplitude[amplitude>0.1])

        # adding it to the amplitude used
        amplitude *= pupil_amplitude

    #-----------------------------------------------------------------
    # Setting the mode basis that will be used for aberration estimation
    #-----------------------------------------------------------------    
    print('\nestimating ' + str(Nmodes) + ' modes')
    print('generating mode basis')
     
    if used_mode_basis == 'disk_harmonics':
        mode_basis = make_disk_harmonic_basis(pupil_grid, Nmodes, diameter)

    elif used_mode_basis == 'zernike':
        mode_basis = make_zernike_basis(Nmodes, diameter, pupil_grid, 4) #starts with defocus
    
    else: 
        raise ValueError('Choose a valid mode basis!')

    # Orthogonalizing the mode basis. 
    #mode_basis = sf.orthogonalize_mode_basis(mode_basis, amplitude)
        
    # converting it to an array 
    mode_basis_array = np.array(mode_basis)

    #-----------------------------------------------------------------
    # generating the arrays where we save the data
    #-----------------------------------------------------------------

    # arrays for focal plane and DM command measurement 
    focal_plane          = np.zeros((Nfoc, Nfoc), dtype=float)
    focal_plane_est      = np.zeros((Nfoc, Nfoc), dtype=float)
    focal_plane_res      = np.zeros((Nfoc, Nfoc), dtype=float)
    focal_plane_res_norm = np.zeros((Nfoc, Nfoc), dtype=float)
    measured_phases      = np.zeros((Npup, Npup))
    
    #-----------------------------------------------------------------
    # Measuring the phase
    #-----------------------------------------------------------------
    
    # lists where we save extra parameter measurements
    Nphot_measurements        = []
    background_measurements   = []
    leakage_measurements      = []
    transmission_measurements = []
    V_measurements            = []
    alignment_measurements    = []

    # time measurements
    T_measurements            = []

    time_1 = time.time()
    #-------------------------------------------------------------------
    # saving the image
    #-------------------------------------------------------------------

    focal_plane[:,:] = data

    # making a field of the data
    data = Field(data.ravel(), focal_grid)

    #-------------------------------------------------------------------
    # estimating the background level
    #-------------------------------------------------------------------
    bg_est = measure_background(data)
    
    print('\nnInitial estimated background = ', bg_est) 

    #-------------------------------------------------------------------
    # estimating the number of photons
    #-------------------------------------------------------------------
    data_temp = data.copy()

    # removing the background estimation 
    data_temp -= bg_est

    data_temp[mask] /= T

    est_photon = float(np.sum(data_temp))

    print('\nnInitial estimated photon number = ', est_photon)

    # coordinates of the two PSFs 
    x_1 = -9.66E-5#-1.7E-4 # radians   #-8.09e-05 0.00173
    y_1 = 1.76E-3#1.4E-3 # radians 
    x_2 = 6.37E-5#-x_1 #   8.28e-05 -0.00182
    y_2 = -1.78E-3#-y_1 # 
    x_3 = -9.15e-06#9.95e-06 #-1.13E-5  #Centroid  #-9.15e-06 -8.63e-06
    y_3 = -8.63e-06 #-4.92e-05 #-3.12E-6  # 

    # radius of the circle that will be used to measure I
    radius = 4E-4 # radians

    #-------------------------------------------------------------------
    # estimating the degree of polarization and leakage strength
    #-------------------------------------------------------------------

    # Estimating the degree of polarization
    V = measure_polarization_degree(data,x_1,x_2,y_1,y_2,radius)

    print('\nInitial estimated degree of polarization = ', V)

    L = measure_leakage(data,x_1,x_2,x_3,y_1,y_2,y_3,radius)

    print('\nInitial estimated leakage = ', L)

    #-------------------------------------------------------------------
    # Setting the arguments for measurement and measure
    #------------------------------------------------------------------- 

    # Providing the estimate for the start vector 
    start_vector = np.zeros(mode_basis_array.shape[0] + np.sum(est_parameters))
    
    parameter_counter = 0 

    if est_parameters[0] == True:
        start_vector[parameter_counter] = est_photon

        est_photon_index = parameter_counter

        parameter_counter += 1 

    if est_parameters[1] == True:
        start_vector[parameter_counter] = bg_est

        est_background_index = parameter_counter

        parameter_counter += 1 

    if est_parameters[2] == True:
        start_vector[parameter_counter] = L

        est_leakage_index = parameter_counter

        parameter_counter += 1 

    if est_parameters[3] == True:
        start_vector[parameter_counter] = V

        est_polarization_index = parameter_counter

        parameter_counter += 1 

    if est_parameters[4] == True:
        start_vector[parameter_counter] = T

        est_transmission_index = parameter_counter

        parameter_counter += 1 

    if est_parameters[5] == True:
        start_vector[parameter_counter] = alignment_error

        est_alignment_index = parameter_counter

        parameter_counter += 1 
    
    # Setting the arguments for the forward modelling 
    args = {'data'                        : data,
            'propagator'                  : propagator,
            'focal grid'                  : focal_grid,
            'oversampling factor'         : oversampling_factor,
            'mode basis'                  : mode_basis_array,
            'amplitude vAPP'              : amplitude,
            'phase vAPP'                  : phase,
            'weight map'                  : weight_map,
            'focal plane mask'            : mask,
            'estimate start vector'       : 'given',
            'start vector'                : start_vector,
            'select background'           : False,
            'select wavefront sensing'    : False,
            'estimate photon number'      : est_parameters[0],
            'estimate background'         : est_parameters[1],
            'estimate leakage'            : est_parameters[2],
            'estimate polarization degree': est_parameters[3],
            'estimate mask transmission'  : est_parameters[4],
            'estimate alignment error'    : est_parameters[5], 
            'photon number'               : est_photon,
            'background'                  : bg_est,
            'leakage'                     : L,
            'polarization degree'         : V,
            'mask transmission'           : T,
            'minimization tolerance'      : tolerance,
            'power regularization'        : -3., 
            'wavelength (m)'              : wavelength,
            'include alignment errors'    : include_alignment_errors,
            'alignment error'             : alignment_error,
            }
    
    # estimating the parameter
    estimated_params, _ = fm.estimate_aberration_coefficients(args, analytical_jacobian=use_analytical_jacobian)
    
    # saving the estimated parameters     
    est_param_cube = estimated_params.copy()
    
    print('estimated_parameters = \n', estimated_params)

    # getting only the estimated modal coefficients 
    estimated_coefs = estimated_params[np.sum(est_parameters):]
    
    if est_parameters[0] == True:
        est_photon = estimated_params[est_photon_index]

    if est_parameters[1] == True:
        bg_est = estimated_params[est_background_index]

    if est_parameters[2] == True:
        L = estimated_params[est_leakage_index]

    if est_parameters[3] == True:
        V = estimated_params[est_polarization_index]

    if est_parameters[4] == True:
        T = estimated_params[est_transmission_index]

    if est_parameters[5] == True:
        alignment_error = estimated_params[est_alignment_index]

    # generate a phase screen that is estimated in radians
    estimated_phase_rad = Field(np.sum(mode_basis_array *  estimated_coefs[:, np.newaxis], axis=0), pupil_grid)
    
    # saving the measured phase
    measured_phases[:,:] = estimated_phase_rad.shaped

    # generating the estimated image
    focal_plane_est[:,:] = est_photon * fm.make_PSF_model(amplitude, phase, propagator, focal_grid, oversampling_factor, L, V, T, mask, estimated_phase_rad, wavelength).shaped + bg_est

    if include_alignment_errors:
        temp_focal_est = Field(focal_plane_est[:,:].ravel(), focal_grid)
        
        focal_plane_est[:,:] = sf.simulate_tip_tilt_residuals(temp_focal_est, alignment_error).shaped

    # saving the residuals of the focal plane
    focal_plane_res[:,:] = data.copy().shaped - focal_plane_est[:,:].copy()
    
    focal_plane_res_norm[:,:] = (data.copy().shaped - focal_plane_est[:,:].copy()) / data.copy().shaped
    
    time_2 = time.time()

    print('\niteration took ' + str(time_2 - time_1) + ' sec')

    # appending all the measurements
    T_measurements.append(time_2 - time_1)
    V_measurements.append(V)
    Nphot_measurements.append(est_photon)
    background_measurements.append(bg_est)
    leakage_measurements.append(L)
    transmission_measurements.append(T)
    alignment_measurements.append(alignment_error)
    
    #-----------------------------------------------------------------
    # Saving data cubes 
    #-----------------------------------------------------------------

    write_fits(focal_plane, save_path + 'focal_plane_' + current_time + '.fits')
    write_fits(focal_plane_est, save_path + 'focal_plane_est_' + current_time + '.fits')
    write_fits(focal_plane_res, save_path + 'focal_plane_res_' + current_time + '.fits')
    write_fits(focal_plane_res_norm, save_path + 'focal_plane_res_norm_' + current_time + '.fits')
    write_fits(measured_phases, save_path + 'measured_phase_' + current_time + '.fits')
   
    #-----------------------------------------------------------------
    # Saving other relevant data
    #-----------------------------------------------------------------    
    
    if use_pupil_amplitude:
        save_file = file_pupil_amplitude
    else:
        save_file = 'None'
        
    # creating the dictionary where we save all relevant information 
    info = {'mas / pix'                     : mas_pix,
            'N pupil'                       : Npup,
            'N focal'                       : Nfoc,
            'N modes'                       : Nmodes,
            'used mode basis'               : used_mode_basis,
            'estimate photon number'        : str(args['estimate photon number']),
            'estimate background'           : str(args['estimate background']),
            'estimate leakage'              : str(args['estimate leakage']),
            'estimate polarization degree'  : str(args['estimate polarization degree']),
            'estimate mask transmission'    : str(args['estimate mask transmission']),
            'photon number'                 : Nphot_measurements,
            'background'                    : background_measurements,
            'leakage'                       : leakage_measurements,
            'polarization degree'           : V_measurements,
            'mask transmission'             : transmission_measurements,
            'iteration time (sec)'          : T_measurements,
            'rotation angle (deg)'          : rotation_angle,
            'minimization tolerance'        : args['minimization tolerance'],
            'power regularization'          : args['power regularization'], 
            'oversampling factor'           : oversampling_factor,
            'use pup amplitude measurement' : str(use_pupil_amplitude),
            'file pup amplitude'            : save_file,
            'include alignment errors'      : str(include_alignment_errors),
            'alignment error'               : alignment_measurements,
            'use analytical jacobian'       : str(use_analytical_jacobian),
            }
 
    # saving that dictionary
    sf.save_dict(info, save_path + 'settings_' + current_time + '.json')

    # saving the estimated parameters
    np.savetxt(save_path + 'estimated_parameters_' + current_time + '.txt', est_param_cube)

    # saving the weight map and mask
    save_cube = np.zeros((2, Nfoc, Nfoc))

    save_cube[0,:,:] = weight_map.reshape(Nfoc, Nfoc)
    save_cube[1,:,:] = mask.reshape(Nfoc, Nfoc)

    # saving the weight map and the mask
    write_fits(save_cube, save_path + 'weight_map_mask_' + current_time + '.fits')
    
if __name__ == '__main__':

    # -------------------------------------------------------------------
    # Generating artificial data 
    # -------------------------------------------------------------------
    import generate_data as gd
    
    # Directory and file names for SCExAO vAPP Design that we use for 
    # testing the code 
    file_vAPP_amplitude = 'GMT_asymmetric_pupil_amp_gvAPP_med_res.fits'
    file_vAPP_phase     = 'GMT_asymmetric_pupil_phase_gvAPP_med_res.fits'

    # Loading the phase and amplitude of the vAPP. 
    amplitude_vAPP  = read_fits(file_vAPP_amplitude)
    phase_vAPP      = read_fits(file_vAPP_phase)

    wavelength = 3 * 532.E-9 # meter

    # diameter telescope 
    diameter   = 6E-3 # meter 

    # lambda / diameter 
    ld = wavelength / diameter # radians
    ld = np.degrees(ld) * 3600. * 1000 # milli arcsec

    # number of pixels along on axis in the focal and pupil planes
    Npup = 300 
    Nfoc = 400 

    # building the grids 
    pupil_grid = make_pupil_grid(Npup, diameter)

    mas_pix = 3.73E3

    rad_pix = np.radians(mas_pix/ 1000 / 3600)
    
    focal_grid = make_uniform_grid([Nfoc, Nfoc], [Nfoc*rad_pix, Nfoc*rad_pix])

    oversampling_factor_data = 1
    focal_grid_sup_data      = make_supersampled_grid(focal_grid, oversampling_factor_data)
    propagator_data          = FraunhoferPropagator(pupil_grid, focal_grid_sup_data)

    # Turning the files into fields.
    amplitude_vAPP = Field(amplitude_vAPP.ravel(), pupil_grid)
    phase_vAPP     = Field(phase_vAPP.ravel(), pupil_grid) 

    ####################################################################
    # WE CREATE TEST DATA 
    ####################################################################

    fp_mask = np.ones(Nfoc*Nfoc, dtype=bool)#ND_mask(Nfoc)

    # setting the weight map 
    weight_map = np.ones(Nfoc*Nfoc)
    """
    rms_wfe = 0.6 # rad 

    
    #Making phase aberrations.
    phase_aberration = gd.make_zernike_phase_screen(pupil_grid, amplitude_vAPP, 10, rms_wfe, wavelength, diameter, remove_tip_tilt=True)
    
    additional_options = {'add aberration'         : True,
                          'aberration'             : phase_aberration,
                          'photon number'          : 1E7,
                          'add photon noise'       : True,
                          'background'             : 10,
                          'read noise'             : 0,
                          'flat field error'       : 0.,
                          'saturation level'       : None,
                          'save data'              : False,
                          'save path'              : 'test/',
                          'leakage'                : 1E-2,
                          'polarization degree'    : 0,
                          'add focal plane mask'   : False, 
                          'focal plane mask'       : fp_mask, 
                          'rms wfe'                : rms_wfe,
                          'vAPP files path'        : '/',
                          'vAPP amplitude file'    : file_vAPP_amplitude,
                          'vAPP phase file'        : file_vAPP_phase,
                          'wavelength'             : wavelength,
                          'oversampling factor'    : oversampling_factor_data,
                          'focal grid'             : focal_grid,
                          }

    # data = gd.make_artifical_data(amplitude_vAPP, phase_vAPP, propagator_data, additional_options)
    """
    data = read_fits('cropped_reduced_img.fits')
    data, _ = imgarr_to_field(data)

    #data = sf.simulate_tip_tilt_residuals(data, 0.5)

    data = Field(data, focal_grid)

    # plt.figure()
    # imshow_field(np.log10(data / data.max()), vmin=-5, vmax=0)
    # plt.colorbar()
    # plt.show()

    # -------------------------------------------------------------------
    # Setting the model parameters 
    # -------------------------------------------------------------------

    # rot angle originally -5,  1) -5.8
    # Wavelenth originally = 1.225 * 532e-9
    # Current best is 1.235 * 532

    model_parameters = {'mas / pix'           : mas_pix,
                        'rotation angle (deg)' : -5,
                        'wavelength (meter)'   : 1.24 * 532E-9,
                        'diameter (meter)'     : 6E-3,
                        'Npix pupil plane'     : 600,
                        'Npix focal plane'     : 400,
                        'Amplitude file'       : 'GMT_asymmetric_pupil_amp_gvAPP_med_res.fits',
                        'Phase file'           : 'GMT_asymmetric_pupil_phase_gvAPP_med_res.fits',
                        }

    # -------------------------------------------------------------------
    # setting algorithm parameters 
    # -------------------------------------------------------------------

    # parameter for optimization step
    tolerance=1E-4

    # Number of modes that will be estimated.   
    Nmodes = 12

    # choosing the mode basis
    used_mode_basis = 'zernike'#'disk_harmonics'#
    
    # using a measurement of the pupil amplitude 
    use_pupil_amplitude = False

    # fitting tip/tilt errors 
    include_alignment_errors = False

    oversampling_factor = 1

    est_photon_number = True 
    est_background    = True 
    est_leakage       = True
    est_pol_degree    = True
    est_mask_trans    = False
    est_align_error   = False

    estimated_parameters = np.array([est_photon_number, est_background, est_leakage, est_pol_degree, est_mask_trans, est_align_error])

    use_analytical_jacobian = True

    # -------------------------------------------------------------------
    # estimating the phase 
    # -------------------------------------------------------------------

    estimate_wavefront(data.shaped, model_parameters, Nmodes, used_mode_basis, tolerance, use_pupil_amplitude, include_alignment_errors, oversampling_factor,estimated_parameters, use_analytical_jacobian)
