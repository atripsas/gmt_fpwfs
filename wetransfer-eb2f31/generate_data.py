'''
This is the second version of the code that generates artifical vAPP data for the forward 
modelling code. 
'''
import numpy as np
import matplotlib.pyplot as plt 
import json

from hcipy import * 

import support_functions as sf 

def simple_psd(grid, alpha=1, n=-2):
    u = grid.as_('polar').r + 1e-20

    #Calculating the psd.
    res = alpha * u ** n

    res[u < 1e-19] = 0
            
    return Field(res, grid)

def make_PSD_phase_screen(pupil_grid, aperture, rms_wavefront_error, power=-2, remove_tip_tilt=True):
    '''
    This function creates a random phase screen created by a PSD that is
    an certain power law with a desired rms wavefront error.

    rms_wavefront_error = in radians
    power = power in the power law.
    '''

    #Making it a wavefront.
    pupil_wavefront = Wavefront(aperture)

    #Creating the phase screen generator. 
    #phase_screen_generator = SpectralNoiseFactoryMultiscale(simple_psd, pupil_grid, 8, psd_args=(1, power))
    phase_screen_generator = SpectralNoiseFactoryFFT(simple_psd, pupil_grid, 1, psd_args=(1, power))

    #Generating the random phase screen.
    phase_screen = phase_screen_generator.make_random().__call__()

    #Multiplying with the aperture function, this will help us set the 
    #rms. 
    phase_screen *= aperture

    #Removing the piston (and tip/tilt) from the phasescreen
    
    #Here we make the mode basis that we remove from the phase screen.
    if remove_tip_tilt:
        mode_basis = make_zernike_basis(3, 1, pupil_grid, 1)
    else: 
        mode_basis = make_zernike_basis(1, 1, pupil_grid, 1)
        
    #Orthogonalizing the mode basis. 
    #mode_basis = sf.orthogonalize_mode_basis(mode_basis, aperture)
    
    #Projecting the phase screen on the piston and tip/tilt modes.
    coeffs = np.dot(inverse_truncated(mode_basis.transformation_matrix), phase_screen)
    
    #Removing the unwanted modes.
    for coefficient, mode in zip(coeffs, mode_basis):
        phase_screen -= coefficient * mode 
            
    #Setting the rms wavefront error.
    phase_screen /= sf.rms(phase_screen, aperture) 
    phase_screen *= rms_wavefront_error
    
    #Adding the phase screen to the wavefront.
    pupil_wavefront.electric_field *= np.exp(1j * phase_screen)

    return pupil_wavefront

def make_zernike_phase_screen(pupil_grid, aperture, number_of_modes, rms_wavefront_error, wavelength, diameter, remove_tip_tilt=True):
    '''
    This function creates a phase screen that consists of purely Zernike modes.
    '''
    #Making it a wavefront.
    pupil_wavefront = Wavefront(aperture, wavelength=wavelength)

    if rms_wavefront_error > 0.:

        #Here we make the mode basis that we remove from the phase screen.
        if remove_tip_tilt:
            mode_basis_temp = make_zernike_basis(number_of_modes, diameter, pupil_grid, 4)
        else: 
            mode_basis_temp = make_zernike_basis(number_of_modes, diameter, pupil_grid, 2)

        #Orthogonalizing the mode basis. 
        mode_basis_ortho = mode_basis_temp#sf.orthogonalize_mode_basis(mode_basis_temp, aperture)
        
        #The random amplitudes of the modes (between -1 and 1).
        coeffs = 2 * np.random.rand(number_of_modes) - 1
        
        #The field where we save the final phase screen. 
        phase_screen = np.zeros_like(aperture)

        #Adding the modes.
        for coefficient, mode in zip(coeffs, mode_basis_ortho):
            phase_screen += coefficient * mode 
                
        #Setting the rms wavefront error.
        phase_screen /= sf.rms(phase_screen, aperture) 
        phase_screen *= rms_wavefront_error
        
        #Adding the phase screen to the wavefront.
        pupil_wavefront.electric_field *= np.exp(1j * phase_screen)

    return pupil_wavefront

def make_power_law_mode_basis(pupil_grid, mode_basis, aperture, rms_wavefront_error, power=-2, remove_tip_tilt=True):
    '''
    This function creates a random phase screen created by a PSD that is
    an certain power law with a desired rms wavefront error.

    rms_wavefront_error = in radians
    power = power in the power law.
    '''

    #Making it a wavefront.
    pupil_wavefront = Wavefront(aperture)

    #Creating the phase screen generator. 
    #phase_screen_generator = SpectralNoiseFactoryMultiscale(simple_psd, pupil_grid, 8, psd_args=(1, power))
    phase_screen_generator = SpectralNoiseFactoryFFT(simple_psd, pupil_grid, 1, psd_args=(1, power))

    #Generating the random phase screen.
    phase_screen = phase_screen_generator.make_random().__call__()

    #Multiplying with the aperture function, this will help us set the 
    #rms. 
    phase_screen *= aperture

    #Removing the piston (and tip/tilt) from the phasescreen
    
    #Here we make the mode basis that we remove from the phase screen.
    if remove_tip_tilt:
        mode_basis_zernike = make_zernike_basis(3, 1, pupil_grid, 1)
    else: 
        mode_basis_zernike = make_zernike_basis(1, 1, pupil_grid, 1)
        
    #Orthogonalizing the mode basis. 
    mode_basis_zernike= sf.orthogonalize_mode_basis(mode_basis_zernike, aperture)
    
    #Projecting the phase screen on the piston and tip/tilt modes.
    coeffs_zernike = np.dot(inverse_truncated(mode_basis_zernike.transformation_matrix), phase_screen)
    
    #Removing the unwanted modes.
    for coefficient, mode in zip(coeffs_zernike, mode_basis_zernike):
        phase_screen -= coefficient * mode 

    # now we project the remaining phase screen on the desired mode basis. 
    coeffs_mode_basis = np.dot(inverse_truncated(mode_basis.transformation_matrix), phase_screen)

    mode_basis_array = np.array(mode_basis)

    # building the phase screen that will go out of it 
    phase_screen_out = np.sum(mode_basis_array * coeffs_mode_basis[:,np.newaxis], axis=0)

    #Setting the rms wavefront error.
    phase_screen_out /= sf.rms(phase_screen_out, aperture) 
    phase_screen_out *= rms_wavefront_error
    
    phase_screen_out = Field(phase_screen_out, pupil_grid)

    #Making it a wavefront.
    pupil_wavefront = Wavefront(aperture * np.exp(1j * phase_screen_out))

    return pupil_wavefront


"""    
def make_disk_harmonics_phase_screen(pupil_grid, aperture, number_of_modes, rms_wavefront_error, remove_tip_tilt=True):
    '''
    This function creates a phase screen that consists of purely Zernike modes.
    '''
    #Making it a wavefront.
    pupil_wavefront = Wavefront(aperture)

    #Here we make the mode basis that we remove from the phase screen.
    if remove_tip_tilt:
        mode_basis = make_zernike_basis(number_of_modes, 1, pupil_grid, 4)
    else: 
        mode_basis = make_zernike_basis(number_of_modes, 1, pupil_grid, 2)

    #Orthogonalizing the mode basis. 
    mode_basis = sf.orthogonalize_mode_basis(mode_basis, aperture)
    
    #The random amplitudes of the modes (between -1 and 1).
    coeffs = 2 * np.random.rand(number_of_modes) - 1
    
    #The field where we save the final phase screen. 
    phase_screen = np.zeros_like(aperture)

    #Adding the modes.
    for coefficient, mode in zip(coeffs, mode_basis):
        phase_screen += coefficient * mode 
            
    #Setting the rms wavefront error.
    phase_screen /= sf.rms(phase_screen, aperture) 
    phase_screen *= rms_wavefront_error
    
    #Adding the phase screen to the wavefront.
    pupil_wavefront.electric_field *= np.exp(1j * phase_screen)

    return pupil_wavefront
"""
def make_artifical_data(amplitude, phase, propagator, additional_options):
    '''
    This function generates artificial data for the forward modelling algorithm.

    amplitude = hcipy field
    phase     = hcipy field
    additional_options = dictionary, consists of the following options: 
    
    'leakage'              = [0,1]
    'polarization degree'  = [0,1]
    'add aberration'       = boolean
    'aberration'           = wavefront with aberration 
    'photon number'        = number of photons 
    'background'           = background contribution 
    'add photon noise'     = boolean 
    'read noise'           = read noise contribution
    'flat field error'     = mean flatfield error 
    'saturation level'     = adds a saturation level 
    'save data'            = boolean 
    'save path'            = path where to save 
    'add focal plane mask' = boolean 
    'focal plane mask'     = the focal plane mask 
    
    example dictionary:
    additional_options = {'add aberration'      : False,
                          'aberration'          : None,
                          'photon number'       : 1,
                          'add photon noise'    : False,
                          'background'          : 0.,
                          'read noise'          : 0.,
                          'flat field error'    : 0.,
                          'saturation level'    : None,
                          'save data'           : False,
                          'save path'           : '/',
                          'leakage'             : 0.,
                          'polarization degree' : 0.,
                          'add focal plane mask': False, 
                          'focal plane mask'    : mask, 
                          'rms wfe'             : rms_wfe,
                          'vAPP files path'     : path_to_vAPP,
                          'vAPP amplitude file' : file_amplitude_vAPP,
                          'vAPP phase file'     : file_phase_vAPP
                          } 
    '''
    # unpacking some relevant stuff for the vAPP creation 
    L = additional_options['leakage']
    V = additional_options['polarization degree']

    wavelength = additional_options['wavelength']

    oversampling_factor = additional_options['oversampling factor']

    focal_grid = additional_options['focal grid']
    

    # Generating the pupil electric fields for the coronagraphic and leakage PSFs.
    pupil_wf_polarization_1 = Wavefront(amplitude * np.exp(1j * phase), wavelength=wavelength)
    pupil_wf_polarization_2 = Wavefront(amplitude * np.exp(-1j * phase), wavelength=wavelength)
    pupil_wf_leakage        = Wavefront(amplitude, wavelength=wavelength)

    # Adding aberrations (can be amplitude and phase). 
    if additional_options['add aberration']:
        pupil_wf_polarization_1.electric_field *= additional_options['aberration'].electric_field
        pupil_wf_polarization_2.electric_field *= additional_options['aberration'].electric_field
        pupil_wf_leakage.electric_field        *= additional_options['aberration'].electric_field

    # Propagating to the focal plane. 
    focal_wf_polarization_1 = propagator(pupil_wf_polarization_1)
    focal_wf_polarization_2 = propagator(pupil_wf_polarization_2)
    focal_wf_leakage        = propagator(pupil_wf_leakage)
    
    # normalizing the sum of the PSFs to 1
    focal_wf_polarization_1.total_power = 1 
    focal_wf_polarization_2.total_power = 1 
    focal_wf_leakage.total_power        = 1 

    # Generating the PSFs and adding polarization and leakage effects 
    PSF_polarization_1 = focal_wf_polarization_1.power * (1 + V) / 2 * (1 - L)
    PSF_polarization_2 = focal_wf_polarization_2.power * (1 - V) / 2 * (1 - L)
    PSF_leakage        = focal_wf_leakage.power * L 

    # Combing all PSFs. 
    PSF_total = PSF_polarization_1 + PSF_polarization_2 + PSF_leakage

    PSF_total = subsample_field(PSF_total, oversampling_factor, focal_grid) 

    if additional_options['photon number'] != None:
        # Normalizing the PSF such that the sum is 1. 
        PSF_total /= np.sum(PSF_total)
        
        # adding a focal plane mask 
        if additional_options['add focal plane mask']:
            PSF_total *= additional_options['focal plane mask']
            
        # Multiplying with total photon number.
        PSF_total *= additional_options['photon number']

        # Adding a thermal / dark current background contribution.
        if additional_options['background'] != None:
            PSF_total += additional_options['background']

        # Adding photon noise.
        if additional_options['add photon noise']:
            PSF_total = large_poisson(PSF_total, thresh=1e6)

        # Adding (Gaussian) read noise.
        if additional_options['read noise'] != None:
            PSF_total += np.random.normal(loc=0, scale=additional_options['read noise'], size=PSF_total.size)
        
        # Adding (Gaussian) flat field error.
        if additional_options['flat field error'] != None:
            PSF_total *= np.random.normal(loc=1.0, scale=additional_options['flat field error'], size=PSF_total.size)

        # Adding maximum saturation level. 
        if additional_options['saturation level'] != None:
            PSF_total[PSF_total > additional_options['saturation level']] = additional_options['saturation level']

    else:
        # Scaling such that the maximum is 1. 
        PSF_total /= PSF_total.max()

   

    if additional_options['save data']:
        # First we check if the save path is available.
        sf.check_directory(additional_options['save path'])

        from time import gmtime, strftime
        k = strftime("%Y-%m-%d_%H-%M-%S", gmtime())

        # First we save the artificial data. 
        write_fits(PSF_total, additional_options['save path'] + 'artificial_data_' + k + '.fits')

        # We save the abberation. 
        aberration_amp   = additional_options['aberration'].amplitude.shaped
        aberration_phase = additional_options['aberration'].phase.shaped
        aberration_out = np.zeros((2,aberration_amp.shape[0],aberration_amp.shape[1]))
        aberration_out[0,:,:] = aberration_amp
        aberration_out[1,:,:] = aberration_phase
        write_fits(aberration_out, additional_options['save path'] + 'aberration_' + k + '.fits')

        # We save the additional information. 
        # First we delete the aberration files. 
        del additional_options['aberration']
        del additional_options['focal plane mask']
        json.dump(additional_options, open(additional_options['save path'] + 'additional_information_' + k + '.txt', 'w'))

    else:
        return PSF_total

def make_PSF_model(amplitude, phase, propagator, L, V, save_path, save_name):
    
    # Generating the pupil electric fields for the coronagraphic and leakage PSFs.
    pupil_wf_polarization_1 = Wavefront(amplitude * np.exp(1j * phase))
    pupil_wf_polarization_2 = Wavefront(amplitude * np.exp(-1j * phase))
    pupil_wf_leakage        = Wavefront(amplitude)

    # Propagating to the focal plane. 
    focal_wf_polarization_1 = propagator(pupil_wf_polarization_1)
    focal_wf_polarization_2 = propagator(pupil_wf_polarization_2)
    focal_wf_leakage        = propagator(pupil_wf_leakage)

    # normalizing the sum of the PSFs to 1
    focal_wf_polarization_1.total_power = 1 
    focal_wf_polarization_2.total_power = 1 
    focal_wf_leakage.total_power        = 1 
    
    # Generating the PSFs and adding polarization and leakage effects 
    PSF_polarization_1 = focal_wf_polarization_1.power * (1 + V) / 2 * (1 - L)
    PSF_polarization_2 = focal_wf_polarization_2.power * (1 - V) / 2 * (1 - L)
    PSF_leakage        = focal_wf_leakage.power * L 

    # Combing all PSFs. 
    PSF_total = PSF_polarization_1 + PSF_polarization_2 + PSF_leakage

    # Scaling such that the maximum is 1. 
    PSF_total /= PSF_total.max()

    # First we check if the save path is available.
    sf.check_directory(save_path)

    write_fits(PSF_total, save_path + save_name)

if __name__ == '__main__':
    '''
    Required arguments:

    add_aberration [boolean]
    aberration

    add_vibration [boolean]
    vibration information
    monochromatic [boolean] 
    bandwidth 
    add_planet [boolean]
    planet position
    planet relative brightness

    out:
    - artificial data [should be in same format as real data]
    - txt with args 
    - fits file with reference PSF, wavefront, etc 

    '''
        
    #Directory and file names for SCExAO vAPP Design
    path_to_vAPP        = '../../vAPP_Designs/SCExAO/'
    file_amplitude_vAPP = 'SCExAO_vAPP_amplitude.fits'
    file_phase_vAPP     = 'SCExAO_vAPP_phase_11.fits'

    #The amplitude and phase of the vAPP loaded. 
    amplitude_vAPP = read_fits(path_to_vAPP + file_amplitude_vAPP)[130:1870, 130:1870]
    phase_vAPP     = read_fits(path_to_vAPP + file_phase_vAPP)[130:1870, 130:1870]

    #Properties
    N = amplitude_vAPP.shape[0]

    #Defining the grids.
    pupil_grid = make_pupil_grid(N)
    focal_grid = make_focal_grid(pupil_grid, q=4, num_airy=25)
    
    propagator = FraunhoferPropagator(pupil_grid, focal_grid)

    #Making the fields
    amplitude_vAPP = Field(amplitude_vAPP.ravel(), pupil_grid)
    phase_vAPP     = Field(phase_vAPP.ravel(), pupil_grid)

    rms_wfe = 0.3

    #Making phase aberrations.
    phase_aberration = make_PSD_phase_screen(pupil_grid, amplitude_vAPP, rms_wfe, power=-3, remove_tip_tilt=True)
    #phase_aberration = make_zernike_phase_screen(pupil_grid, amplitude_vAPP, 10, rms_wfe, remove_tip_tilt=True)
    leakage = 0.025

    #phase_aberration.electric_field *= np.exp(2j * np.pi * (pupil_grid.x * 5 + pupil_grid.y * 5))
    
    # add focal plane mask 
    def ND_mask(grid):
        mask = grid.rotated(np.radians(-25 - 69)).x < 0.1
        mask += grid.rotated(np.radians(-70 - 69 )).x > 0.1
         
        mask2 = grid.rotated(np.radians(-73 + 180 - 69)).x > -0.1
        mask2 += grid.rotated(np.radians(-28 + 180 - 69)).x < -0.1
         
        mask3 = 1 - circular_aperture(8)(grid)
        masktot = mask * mask2 * mask3
     
        mask_out = Field(np.zeros_like(masktot, dtype=bool), grid)
        mask_out[masktot == False] = True
     
        return mask_out
     
    fp_mask = Field(np.ones_like(focal_grid.x), focal_grid)
    fp_mask[ND_mask(focal_grid)] = 0.04
    
    plt.figure()
    imshow_field(fp_mask)
    
    #plt.show()
     
    additional_options = {'add aberration': True,
                          'aberration': phase_aberration,
                          'photon number': 1E7,
                          'add photon noise': True,
                          'background': 100,
                          'read noise': 10,
                          'flat field error': 0.01,
                          'saturation level': None,
                          'save data': True,
                          'save path': 'test/',
                          'leakage': leakage,
                          'polarization degree': 0.03,
                          'add focal plane mask': True, 
                          'focal plane mask': fp_mask, 
                          'rms wfe': rms_wfe,
                          'vAPP files path': path_to_vAPP,
                          'vAPP amplitude file': file_amplitude_vAPP,
                          'vAPP phase file': file_phase_vAPP
                          }

    make_artifical_data(amplitude_vAPP, phase_vAPP, propagator, additional_options)
    make_PSF_model(amplitude_vAPP, phase_vAPP, propagator, leakage, 0, 'test/', 'SCExAO_vAPP_model.fits')



