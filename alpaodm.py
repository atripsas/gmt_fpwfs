from hcipy import *
import numpy as np
from matplotlib import pyplot as plt
import time

import zmq
import json

class LEXI_DM():

	def __init__(self):	
		# The vector which holds the DM commands
		self._actuators = np.zeros((97,))
				
		# Make the grid on which all modes are determined
		self.pupil_grid = make_pupil_grid(256)
		
		# This keeps track of which modes are present
		self.mode_basis = {}
		
		# Make the standard modes in which the DM can be operated
		self.make_poke_modes()
		self.sort_actuators_by_radius()
		
		self.make_zernike_modes()
		
		# Set the default basis mode
		self.current_basis = 'Poke'	
		self._mode_coefficients = np.zeros((97,))

	# Open the connection to LEXIAO
	def connect(self, context, port):
		#self.context = context
		#self.dm_publisher = self.context.socket(zmq.PUB)
		#address = sprintf('tcp://161.72.36.238:%d', port);
		#self.dm_publisher.connect("tcp://169.254.167.223:{:d}".format(port))
		#self.dm_publisher.bind("tcp://169.254.149.140:{:d}".format(port))
		#self.dm_publisher.bind("tcp://161.72.36.238:{:d}".format(socket))

		self.context = context
		self.dm_publisher = self.context.socket(zmq.REQ)
		#port = 5555
		#self.dm_publisher.connect("tcp://169.254.167.223:{:d}".format(port))		
		self.dm_publisher.connect("tcp://expoao.ing.iac.es:{:d}".format(port))		
		
	# Sends the current dm shape commands to the AO system
	def actuate(self):
		# Convert the modal coefficients to actuator coefficients
		#voltages = self.mode_basis[self.current_basis]['to_actuator_space'].dot(self._actuators)
		#temp_act_coef = self.mode_basis[self.current_basis]['to_actuator_space'].dot(self._actuators)
		voltages = self._actuators.copy()
		
		# Convert to string
		command_msg = "num_modes {:d} amps".format(97)
		for acti in voltages:
			command_msg += " {:f}".format(acti)

		# Send to LEXIAO
		#print(command_msg)
		self.dm_publisher.send(command_msg.encode('UTF-8'))
			
		message_reply = self.dm_publisher.recv()	
		#print(message_reply)
		

	def make_poke_modes(self):
		x = np.linspace(-0.5,0.5,11)
		XX, YY = np.meshgrid(x, x)
		
		act_map = np.array([
		[0,0,0,66,55,44,33,22,0,0,0],
		[0,0,77,67,56,45,34,23,13,0,0],
		[0,86,78,68,57,46,35,24,14,6,0],
		[93,87,79,69,58,47,36,25,15,7,1],
		[94,88,80,70,59,48,37,26,16,8,2],
		[95,89,81,71,60,49,38,27,17,9,3],
		[96,90,82,72,61,50,39,28,18,10,4],
		[97,91,83,73,62,51,40,29,19,11,5],
		[0,92,84,74,63,52,41,30,20,12,0],
		[0,0,85,75,64,53,42,31,21,0,0],
		[0,0,0,76,65,54,43,32,0,0,0]]).ravel()
			
		x = np.zeros((97,))
		y = np.zeros((97,))
		for i, xi in enumerate(XX.ravel()):
			if( act_map[i]-1 >= 0 ):
				x[act_map[i]-1] = xi
		
		for i, yi in enumerate(YY.ravel()):
			if( act_map[i]-1 >= 0 ):
				y[act_map[i]-1] = yi
		
		self.act_grid = CartesianGrid(UnstructuredCoords((x,y)))
		
		pokes = make_gaussian_pokes(self.pupil_grid, self.act_grid, 1/(11-1))
		self.mode_basis['Poke'] = {
			'basis' : pokes,
			'to_phase_space' : pokes.transformation_matrix,
			'to_actuator_space' : np.eye(97),
			'num_modes' : 97
		}
		
	def sort_actuators_by_radius(self):
		index_map = np.array([
		[0,0,0,66,55,44,33,22,0,0,0],
		[0,0,77,67,56,45,34,23,13,0,0],
		[0,86,78,68,57,46,35,24,14,6,0],
		[93,87,79,69,58,47,36,25,15,7,1],
		[94,88,80,70,59,48,37,26,16,8,2],
		[95,89,81,71,60,49,38,27,17,9,3],
		[96,90,82,72,61,50,39,28,18,10,4],
		[97,91,83,73,62,51,40,29,19,11,5],
		[0,92,84,74,63,52,41,30,20,12,0],
		[0,0,85,75,64,53,42,31,21,0,0],
		[0,0,0,76,65,54,43,32,0,0,0]])
		
		act_map = index_map > 0
		XX, YY = np.meshgrid(np.arange(11)-5, np.arange(11)-5)
		
		self.sorted_indices = []
		for ri in range(6):
			ring_map = (act_map * ((abs(XX) == ri) * (abs(YY)<=ri) + (abs(YY) == ri) * (abs(XX)<=ri) ) )>0		
			self.sorted_indices.append( index_map.ravel()[ring_map.ravel()] )
			#print(self.sorted_indices[-1])
			
	def make_zernike_modes(self):
		#self.act_grid
		zmodes = make_zernike_basis(96, 1, self.act_grid,starting_mode=2)
		self.add_basis('Zernike', zmodes, 96)
		
	def add_basis(self, basis_name, new_mode_basis, num_modes, is_actmap=False):
		#inverse_tikhonov(self.mode_basis['Poke']['to_phase_space'], 0.5).dot(new_mode_basis.transformation_matrix)
		
		if is_actmap:
			self.mode_basis[basis_name] = {
				'basis' : new_mode_basis,
				'to_phase_space' : self.mode_basis['Poke']['to_phase_space'].dot( new_mode_basis.transformation_matrix ),	# Mode coef to phase space
				'to_actuator_space' : new_mode_basis.transformation_matrix,
				'to_mode_space' : inverse_tikhonov(new_mode_basis.transformation_matrix, 1E-4),
				'num_modes' : num_modes
			}
		else:
			self.mode_basis[basis_name] = {
				'basis' : new_mode_basis,
				'to_phase_space' : self.mode_basis['Poke']['to_phase_space'].dot( new_mode_basis.transformation_matrix ),	# Mode coef to phase space
				'to_actuator_space' : new_mode_basis.transformation_matrix,
				'to_mode_space' : inverse_tikhonov(new_mode_basis.transformation_matrix, 1E-4),
				'num_modes' : num_modes
			}
	
	def get_mode_profile(self, index, asked_basis=None):
		#temp_act_coef = self.mode_basis[self.current_basis]['to_actuator_space'].dot(self._actuators)
		mode = self.mode_basis[self.current_basis]['to_actuator_space'][:, index]
		temp_surface = self.mode_basis['Poke']['to_phase_space'].dot(mode)
		return Field(temp_surface, self.pupil_grid)
	
	def reset(self):
		self._actuators = np.zeros((97,))
	
	@property
	def tip(self):
		return self.mode_basis['Zernike']['to_actuator_space'][:,0]
		
	@property
	def tilt(self):
		return self.mode_basis['Zernike']['to_actuator_space'][:,1]
		
	@property
	def focus(self):
		return self.mode_basis['Zernike']['to_actuator_space'][:,2]
	
	@property
	def actuators(self):
		return self._actuators
	
	@actuators.setter
	def actuators(self, new_actuator_command):
		if new_actuator_command.size == 97:
			self._actuators = np.clip(new_actuator_command, -1, 1)
	
	def add_command(self, new_command):
		if new_command.size == 97:
			self._actuators = np.clip(self._actuators + new_command, -1, 1)
	
	def set_actuator(self, index, value):
		if index < 97:
			if value > 1:
				self._actuators[index] = 1
			elif value < -1:
				self._actuators[index] = -1
			else:
				self._actuators[index] = value
	
	def add_actuator(self, index, value):
		if index < 97:
			self._actuators[index] += value
			self._actuators = np.clip(self._actuators, -1, 1)

	# Modal control
	def add_mode(self, index, amp):
		if index < self.num_modes():
			new_command = np.zeros((self.num_modes(),))
			self.add_command( self.mode_basis[self.current_basis]['to_actuator_space'][:,index] * amp )

	def add_modes(self, new_command):
		if new_command.size == self.num_modes():
			self.actuators = self.actuators + self.mode_basis[self.current_basis]['to_actuator_space'].dot(new_command)
	
	def set_mode(self, index, amp):
		if index < self.num_modes():
			new_command = np.zeros((self.num_modes(),))
			self.actuators = self.mode_basis[self.current_basis]['to_actuator_space'][:,index] * amp
	
	def set_modes(self, new_command):
		if new_command.size == self.num_modes():
			self.actuators = self.mode_basis[self.current_basis]['to_actuator_space'].dot(new_command)
	
	def num_modes(self):
		return self.mode_basis[self.current_basis]['num_modes']
			
	@property
	def basis(self):
		return self.current_basis
	
	@basis.setter
	def basis(self, new_basis):
		'''
			This function determines in which basis the DM is operated.
		'''
		if new_basis in self.mode_basis:
			self.current_basis = new_basis
	
	@property
	def surface(self):
		# in phase
		#temp_act_coef = self.mode_basis[self.current_basis]['to_actuator_space'].dot(self._actuators)
		temp_surface = self.mode_basis['Poke']['to_phase_space'].dot(self._actuators)
		return Field(temp_surface, self.pupil_grid)
		
if __name__ == "__main__":
	context = zmq.Context()
	
	dm = LEXI_DM()
	dm.connect(context,5700)
	dm.basis = 'Zernike'

	tip = dm.act_grid.x
	tip /= np.std(tip)
	
	dm.actuators = 0.001 * tip
	
	imshow_field(dm.surface)
	plt.colorbar()
	plt.show()
	
	for i in range(1):
		dm.reset()
		#dm.set_actuator(i, 0.1)
		dm.actuate()
		
		#imshow_field(dm.surface)
		#plt.colorbar()
		#plt.show()
		
		#input('test')