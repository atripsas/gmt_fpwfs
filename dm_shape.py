import numpy as np 
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D

def make_shape(ref=None,image=False):
    '''
    This is for mapping out the approximate shape of
    the ALPAO DM using the LEXI_DM reference
    Input:
    Reference File
    Output:
    3D DM surface map
    '''
    # image = False
    # ref = None
    if ref == None:
        ref = 'DM_ref/sharpening_reference_02.txt'

    index_map = np.array([
                    [0,0,0,66,55,44,33,22,0,0,0],
                    [0,0,77,67,56,45,34,23,13,0,0],
                    [0,86,78,68,57,46,35,24,14,6,0],
                    [93,87,79,69,58,47,36,25,15,7,1],
                    [94,88,80,70,59,48,37,26,16,8,2],
                    [95,89,81,71,60,49,38,27,17,9,3],
                    [96,90,82,72,61,50,39,28,18,10,4],
                    [97,91,83,73,62,51,40,29,19,11,5],
                    [0,92,84,74,63,52,41,30,20,12,0],
                    [0,0,85,75,64,53,42,31,21,0,0],
                    [0,0,0,76,65,54,43,32,0,0,0]])

    DM_voltages = np.loadtxt(ref)

    #Make copy of index map and ravel them
    act_map = index_map.copy()
    act_map = act_map.ravel()

    act_float = act_map.astype(float)

    #applying voltage values to actuator position
    for i in range(len(DM_voltages)):
        pos = np.where(act_map==i+1)
        act_float[pos[0][0]] = DM_voltages[i]

    #reshape to map
    surface = act_float.reshape(np.shape(index_map))

    #plot image of surface
    if image:
        plt.imshow(surface,origin='lower')
        plt.colorbar

    #preping 3d plot data
    X = np.arange(-5,6)
    Y = X
    X,Y = np.meshgrid(X,Y)

    #prepping fig
    fig = plt.figure()
    ax = fig.gca(projection = '3d')
    plt.title('DM Shape')
    ax.plot_surface(X,Y,surface, cmap=cm.coolwarm)
    # fig.colorbar()
    plt.show()

def option_parser():

    import argparse
    from str2bool import str2bool

    parser = argparse.ArgumentParser()
    parser.add_argument('--ref',dest='ref',default=None,
                        help='Input path for reference text file')
    parser.add_argument('-i','--image',dest='image', type=str2bool,
                        default='False', help='Choose whether or not to display 2d image')
    return parser

if __name__ in "__main__":

    
    make_shape()