import numpy as np
from matplotlib import pyplot as plt
from pymba import *
import time
import threading

from astropy.io import fits
from hcipy import *

class FrameManager():
	def __init__(self):
		self.frame_counter = 0
		self.buffer_size = 0
		self.frames = []
		
		self.images = []
		
	def wait_for_exposure(self):
		if self.buffer_size == 1:
			self.frames[0].waitFrameCapture()
			#self.process(self.frames[0])
			
		elif self.buffer_size > 1:
			current_frame_index = self.frame_counter % self.buffer_size
			self.frames[current_frame_index].waitFrameCapture()
			#self.process(self.frames[current_frame_index])
		
	def add_frame(self, new_frame):
		self.frames.append(new_frame)
		self.buffer_size += 1
		
	def get_last_frame(self):
		return self.images[-1]
		
class PIKE():

	def __init__(self):
		self.buffer_size = 0
		self.frame_counter = 0
		
		self.image_counter = 0
		self.image_subset_counter = 0
		self.image_buffer_size = 0
		
		self.frames = []
		self.images = np.zeros((1,512,512))
		
		self.num_stack = 1
		self.wait_for_new_frame = threading.Event()
		
		self.state = 0
		self.is_running = True
		
		
		self.has_background = False

	def init_camera(self, vimba, index):
		cameraIDs = vimba.getCameraIds()
		if len(cameraIDs) > 1:
			print('Multiple cameras detected, selecting first one')
		self.cameraID = cameraIDs[index]
		
		self.camera = vimba.getCamera(self.cameraID)
		self.camera.openCamera()
		
	def print_features(self):
		cameraFeatureNames = self.camera.getFeatureNames()
		for name in cameraFeatureNames:
			print(name)
		
	def state(self):
		state_tree = {}
		
		return state_tree
		
	@property
	def exposure_time(self):
		return self.camera.ExposureTime
	
	@exposure_time.setter
	def exposure_time(self, texp):
		self.camera.ExposureTime = texp
		self.camera.AcquisitionFrameRate = self.camera.AcquisitionFrameRateLimit
		self.has_background = False
		
	@property
	def pixel_format(self):
		return self.camera.PixelFormat
		
	@pixel_format.setter
	def pixel_format(self, new_format):
		self.camera.PixelFormat = new_format
		self.camera.AcquisitionFrameRate = self.camera.AcquisitionFrameRateLimit
		self.has_background = False
		
	@property
	def gain(self):
		return self.camera.Gain
		
	@gain.setter
	def gain(self, new_gain):
		if new_gain < 0:
			new_gain = 0
		elif new_gain > 22.20:
			new_gain = 22.20
		self.camera.Gain = new_gain
		self.has_background = False
		
	@property
	def binning(self):
		if self.camera.IIDCMode == 'Mode0':
			return 1
		elif self.camera.IIDCMode == 'Mode2':
			return 2
		elif self.camera.IIDCMode == 'Mode3':
			return 4
		elif self.camera.IIDCMode == 'Mode4':
			return 8
	
	@binning.setter
	def binning(self, binsize):
		
		if binsize == 1:
			self.camera.IIDCMode = 'Mode0'
		elif binsize == 2: 
			self.camera.IIDCMode = 'Mode2'
		elif binsize == 4:
			self.camera.IIDCMode = 'Mode3'
		elif binsize == 8:
			self.camera.IIDCMode = 'Mode4'
			
		self.camera.OffsetY = 0
		self.camera.OffsetX = 0
		self.camera.Height = self.camera.HeightMax
		self.camera.Width = self.camera.WidthMax
		
		self.camera.AcquisitionFrameRate = self.camera.AcquisitionFrameRateLimit
		self.has_background = False
		
	@property
	def size(self):
		return np.array([self.camera.Height, self.camera.Width])
		
	@size.setter
	def size(self, new_size):
		self.camera.Height = new_size[0]
		self.camera.Width = new_size[1]
		
		self.camera.AcquisitionFrameRate = self.camera.AcquisitionFrameRateLimit
	
	@property	
	def offset(self):
		return np.array([self.camera.OffsetY, self.camera.OffsetX])
	
	@offset.setter
	def offset(self, new_offset):
		self.camera.OffsetY = new_offset[0]
		self.camera.OffsetX = new_offset[1]
		
		self.camera.AcquisitionFrameRate = self.camera.AcquisitionFrameRateLimit
		
	def area(self):
		
		x0 = self.camera.OffsetX
		x1 = self.camera.OffsetX + self.camera.Width
		y0 = self.camera.OffsetY
		y1 = self.camera.OffsetY + self.camera.Height
		
		return [x0, x1, y0, y1]	
	
	@property
	def mode(self):
		if self.camera.AcquisitionMode == 'SingleFrame':
			return 0
		elif self.camera.AcquisitionMode == 'MultiFrame':
			return 1
		elif self.camera.AcquisitionMode == 'Continuous':
			return 2

	@mode.setter
	def mode(self, new_mode):
		if new_mode == 0:
			self.camera.AcquisitionMode = 'SingleFrame'
		elif new_mode == 1:
			self.camera.AcquisitionMode = 'MultiFrame'
		elif new_mode == 2:
			self.camera.AcquisitionMode = 'Continuous'
			
	def prepare_frames(self, number_of_frames, callback=None):
		self.frame_counter = 0
		self.buffer_size = number_of_frames
		self.frames = [self.camera.getFrame() for i in range(number_of_frames)]
				
		# announce frame
		for frame in self.frames:
			frame.announceFrame()
			frame.queueFrameCapture(callback)
		
	def prepare_image_buffer(self, number_of_images):
		self.image_counter = 0
		self.image_buffer_size = number_of_images
		self.images = np.zeros((number_of_images, self.camera.Height, self.camera.Width))
		
	def process(self, new_frame):
		# Get the index of the image
		current_image_index = self.image_counter % self.image_buffer_size

		tarea = self.area()
		if self.image_subset_counter == 0:
			# Overwrite the image if we start a new stack
			self.images[current_image_index] = new_frame.getImage().copy().astype(np.float)		
			if self.has_background:
				self.images[current_image_index] -= self.dark_img[tarea[2]:tarea[3],tarea[0]:tarea[1]]
		else:
			# Othewise add to the image
			self.images[current_image_index] += new_frame.getImage().copy().astype(np.float)
			
			if self.has_background:
				self.images[current_image_index] -= self.dark_img[tarea[2]:tarea[3],tarea[0]:tarea[1]]
		
		# We have added a subimage
		self.image_subset_counter += 1
		
		if self.image_subset_counter == self.num_stack:
			# If we have stacked enough frames we can reset the stacking
			self.image_subset_counter = 0
			
			# And go to the next image
			# TODO : add a mutex lock around this for thread safety
			self.image_counter += 1
			
			self.wait_for_new_frame.set()
		
		# Queue the frame again so that Vimba knows the frame can be used
		new_frame.queueFrameCapture(new_frame._frameCallback)
		
		# Update the internal frame counter
		self.frame_counter += 1
		
	def wait_for_exposure(self):
		self.wait_for_new_frame.wait()#timeout=DELAY)
		self.wait_for_new_frame.clear()
		
	def get_last_image(self):
		current_image_index = (self.image_counter-1) % self.image_buffer_size
		return self.images[current_image_index].copy()
		
	def take_single_exposure(self):
		self.start()
		self.stop()
		
		current_index = self.frame_counter % self.buffer_size
		current_frame = self.frames[current_index]
		
		current_frame.waitFrameCapture()
		current_frame.queueFrameCapture()
		self.frame_counter += 1
		
		current_image_index = self.image_counter % self.image_buffer_size
		
		self.images[current_image_index] = current_frame.getImage().copy().astype(np.float)
		
		tarea = self.area()
		if self.has_background:
			#print("Subtracting som ething")
			self.images[current_image_index] -= self.dark_img[tarea[2]:tarea[3],tarea[0]:tarea[1]]
			
		self.image_counter += 1
				
	def take_contineous_exposure(self, num_frames=1):
		if num_frames > 0:
			current_image_index = self.image_counter % self.image_buffer_size
			
			for i in range(num_frames):
				current_index = self.frame_counter % self.buffer_size
				current_frame = self.frames[current_index]
					
				# For the first image we overwrite it and for second we add
				if i == 0:
					self.images[current_image_index] = current_frame.getImage().copy()
				else:
					self.images[current_image_index] += current_frame.getImage().copy()

				self.frame_counter += 1

			current_image_index += 1
			
	def camera_handler(self):
		while self.is_running:
			
			if self.state == 0:
				time.sleep(0.01)
				
			elif self.state == 1:
				self.take_single_exposure()
				self.state = 0
				
			elif self.state == 2:
				self.start()
				while self.state == 2:
					#self.take_contineous_exposure()
					time.sleep(0.01)
				self.stop()
			
	def startup(self):
		# Set the camera to enable frame capture
		self.camera.startCapture()
		
	def start(self):
		# Start the actual frame capture
		self.camera.runFeatureCommand('AcquisitionStart')
		
	def stop(self):
		# Stop acquiring frames
		self.camera.runFeatureCommand('AcquisitionStop')
		
	def shutdown(self):
		# clean up after capture
		self.camera.endCapture()
		self.camera.flushCaptureQueue()
		self.camera.revokeAllFrames()

		
	def take_dark(self, num_frames, filename):
		self.has_background = False
		
		input("Switch off the laser and press enter...")
		# Take a dark
		self.dark_img = 0
		for i in range(num_frames):
			#print("Take dark img {:d}".format(i))
			self.take_single_exposure()
			self.dark_img += self.get_last_image()
		self.dark_img /= num_frames
		input("Turn on the laser and press enter...")
		
		write_fits(self.dark_img, filename)

		self.has_background = True
		
	def load_dark(self, filename):
		self.dark_img = fits.getdata(filename)
		self.has_background = True
		
if __name__ == "__main__":

	us = 1
	ms = 1E3 * us
	s = 1E6 * us

	vimba = Vimba()
	system = vimba.getSystem()
	vimba.startup() 

	if system.GeVTLIsPresent:
		system.runFeatureCommand("GeVDiscoveryAllOnce")
		time.sleep(2)
	
	cam = PIKE()
	cam.init_camera(vimba, 0)
	
	# Set exposure parameters
	cam.pixel_format = 'Mono16'
	cam.binning = 1
	cam.mode = 0
	buffer_size = 1
	
	
	# Set the frame parameters
	num_steps = 10
	
	dt = np.logspace(-1, 2, 8)
	FPS = np.zeros_like(dt)
	
	for ti, t in enumerate(dt):
		cam.exposure_time = t * ms#10 * ms
		cam.startup()
		
		#cam.frame_counter = 0
		cam.prepare_frames(buffer_size)	
		cam.prepare_image_buffer(buffer_size)	
		
		start = time.time()
		while cam.frame_counter < num_steps:
			cam.take_single_exposure()
		end = time.time()
		#print("FPS {:g}".format(num_steps/(end-start) ))
		
		cam.shutdown()	
		
		FPS[ti] = (1.0*num_steps)/(end-start) 
	
	plt.plot(dt, FPS)
	plt.xscale('log')
	plt.show()
	
	vimba.shutdown()