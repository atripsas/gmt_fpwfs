from pike import *
from alpaodm import *
import numpy as np
from matplotlib import pyplot as plt
from hcipy import *
import time

if __name__ == "__main__":
	us = 1
	ms = 1E3 * us
	s = 1E6 * us

	# Init the DM
	dm = LEXI_DM()
	dm.basis = 'Poke'
	
	#DM Zernike modes or individual actuator pokes
	cal_space = 'Zernike'
	#cal_space = 'Poke'
	
	# Init vimba
	vimba = Vimba()
	system = vimba.getSystem()
	vimba.startup() 

	#No clue AT
	if system.GeVTLIsPresent:
		system.runFeatureCommand("GeVDiscoveryAllOnce")
		time.sleep(2)
	
	# Init the camera
	cam = PIKE()
	cam.init_camera(vimba, 0)
	
	# Set exposure parameters
	cam.pixel_format = 'Mono16'
	cam.binning = 1
	cam.mode = 0
	
	#cam.size = [100, 100]
	#cam.offset = [200, 280]
	
	frame_buffer_size = 50
	image_buffer_size = 50


	# Make the focal plane grid
	pixel_scale = 1.22/3.9 * cam.binning # 3 pixels per lambda/D
	N = np.array([cam.camera.Width, cam.camera.Height])
	cam_size = N * pixel_scale
	focal_grid = make_uniform_grid(N, cam_size)
	fpmask = circular_aperture(5)(focal_grid)
	
	# Set the frame parameters
	cam.exposure_time = 25 * us
	num_frames = 1
	
	
	# Startup the camera
	cam.startup()
	cam.frame_counter = 0
	
	cam.prepare_frames(frame_buffer_size)#, cam.process)
	cam.prepare_image_buffer(image_buffer_size)	
	cam.num_stack = num_frames
	
	img = 0
	#How many frames to integrate over
	for i in range(num_frames):
		cam.take_single_exposure()
		img += cam.get_last_image()#.ravel()
	
	xc = focal_grid.x[np.argmax(img)]
	yc = focal_grid.y[np.argmax(img)]
	
	#cam.take_dark(100, 'master_dark.fits')
	#cam.load_dark('master_dark.fits')
	
	# Start the calibration
	num_modes = 90
	start_mode = 2
	amps = np.linspace(-0.03, 0.03, 91)
	
	#amps = np.logspace(-5, -2, 21)
	#amps = np.concatenate((-amps[::-1], amps))
	
	#start_ref = 20
	#current_reference = np.loadtxt('sharpening_reference_{:d}.txt'.format(start_ref))
	current_reference = np.zeros((97,))
	
	current_max = 0
	
	plt.figure(figsize=(12,4))
	
	#cam.start()
	for k in range(10):

		for i in range(num_modes):
			mode_index = i + start_mode
			print("Calibrate mode {:d}".format(mode_index))

			peak = np.zeros_like(amps)
			
			for ai, amp in enumerate(amps):
				
				control_command = current_reference.copy()
				
				mode_coefs = np.zeros((dm.mode_basis[cal_space]['num_modes'],))
				mode_coefs[mode_index] = amp						
				control_command += dm.mode_basis[cal_space]['to_actuator_space'].dot(mode_coefs)		
				
				dm.actuators = control_command
				dm.actuate()
				
				img = 0
				for i in range(num_frames):
					cam.take_single_exposure()
					img += cam.get_last_image()#.ravel()
				
				#for ki in range(10):
				#	cam.wait_for_exposure()
				#img = cam.get_last_image()
		
				PSF = Field(img.ravel(), focal_grid)
				peak[ai] = PSF.max()
				
				if PSF.max() > current_max:
					current_max = PSF.max()
				
				if True:
					if ai % 5 == 0:
						plt.clf()
						
						plt.subplot(2,2,1)
						imshow_field( abs(PSF)/current_max, vmin=0, vmax=1)
						plt.xlim([-10+xc,10+xc])
						plt.ylim([-10+yc,10+yc])
						plt.colorbar()
						
						plt.subplot(2,2,2)
						imshow_field( np.log10( abs(PSF)/current_max + 1E-15), vmin=-3, vmax=0)
						plt.xlim([-10+xc,10+xc])
						plt.ylim([-10+yc,10+yc])
						plt.colorbar()
						
						plt.subplot(2,2,3)
						plt.plot(amps, peak , 'C1o-')
						#plt.plot(abs(amps[amps<0]), peak[amps<0], 'C0o-')
						#plt.plot(abs(amps[amps>0]), peak[amps>0], 'C1o-')
						#plt.xscale('log')
						
						plt.subplot(2,2,4)
						imshow_field(dm.surface)
						
						plt.draw()
						plt.pause(0.01)	

			# Set add to the current_reference
			mode_coefs = np.zeros((dm.mode_basis[cal_space]['num_modes'],))
			mode_coefs[mode_index] = amps[np.argmax(peak)]	
			current_reference += dm.mode_basis[cal_space]['to_actuator_space'].dot(mode_coefs)

		np.savetxt("sharpening_reference_{:0>2d}.txt".format(k+1), current_reference)
		
		dm.actuators = current_reference.copy()
		dm.actuate()
		
		img = 0
		for i in range(100):
			cam.take_single_exposure()
			img += cam.get_last_image()#.ravel()
		write_fits(img, 'sharpening_psf_{:0>2d}.fits'.format(k+1))
		
	plt.close()
	
	# Camera shutdown
	cam.shutdown()
	vimba.shutdown()