from pike import *
# from alpaodm import *
import numpy as np
# from matplotlib import pyplot as plt
from hcipy import *
import time

if __name__ == "__main__":
    us = 1
    ms = 1E3 * us
    s = 1E6 * us

    exp_time = 25 * us

    vimba = Vimba()
    system = vimba.getSystem()
    vimba.startup()

    if system.GeVTLIsPresent:
        system.runFeatureCommand("GeVDiscoveryAllOnce")
        time.sleep(2)

    cam = PIKE()
    cam.init_camera(vimba,0)

    cam.pixel_format= 'Mono16'
    cam.binning = 1
    cam.mode = 0

    frame_buffer_size = 50
    image_buffer_size = 50

    cam.exposure_time = exp_time
    num_frames = 1

    cam.startup()
    cam.frame_counter = 0

    cam.prepare_frames(frame_buffer_size)
    cam.prepare_image_buffer(image_buffer_size)
    cam.num_stack = num_frames

    img = 0

    for i in range(num_frames):
        cam.take_single_exposure()
        img += cam.get_last_image()

    date = time.localtime()

    filename = 'snapshot_%i_%i_%i'

    filename = 'snapshot_' + date[0] + '_' + date[1] + '_' + date[2] + '_' +
    for i in range(len(date[:-3])):

