from pike import *
from alpaodm import *
import numpy as np
from matplotlib import pyplot as plt
from hcipy import *
import time

if __name__ == "__main__":
    us = 1
    ms = 1E3 * us
    s = 1E6 * us

    # Init the DM
    dm = LEXI_DM()
    dm.basis = 'Poke'

    cal_space = 'Poke'

    vimba = Vimba()
    system = vimba.getSystem()
    vimba.startup()

    if system.GeVTLIsPresent:
        system.runFeatureCommand("GeVDiscoveryAllOnce")
        time.sleep(2)

    # Init the camera
	cam = PIKE()
	cam.init_camera(vimba, 0)
	
	# Set exposure parameters
	cam.pixel_format = 'Mono16'
	cam.binning = 1
	cam.mode = 0
	
	#cam.size = [100, 100]
	#cam.offset = [200, 280]
	
	frame_buffer_size = 50
	image_buffer_size = 50

	# Make the focal plane grid
	pixel_scale = 1.22/3.9 * cam.binning # 3 pixels per lambda/D
	N = np.array([cam.camera.Width, cam.camera.Height])
	cam_size = N * pixel_scale
	focal_grid = make_uniform_grid(N, cam_size)
	fpmask = circular_aperture(5)(focal_grid)
	
	# Set the frame parameters
	cam.exposure_time = 25 * us
	num_frames = 1

	# Startup the camera
	cam.startup()
	cam.frame_counter = 0
	
	cam.prepare_frames(frame_buffer_size)#, cam.process)
	cam.prepare_image_buffer(image_buffer_size)	
	cam.num_stack = num_frames
	
	img = 0
	#How many frames to integrate over
	for i in range(num_frames):
		cam.take_single_exposure()
		img += cam.get_last_image()#.ravel()
	
	xc = focal_grid.x[np.argmax(img)]
	yc = focal_grid.y[np.argmax(img)]

    amps = np.linspace(-0.9,0.9,50)
	current_reference = np.zeros((97,))

    for i in range(97):
		# Reset reference
        # act = np.where(act_map==i)
        # current_reference = np.zeros((97,))
		act_command = current_reference.copy()

		img = 0
		for p in range(num_frames):
			cam.take_single_exposure()
			img += cam.get_last_image()
		# Take a reference image to compare to
		PSF_ref = Field(img.ravel(), focal_grid)

		PSF_poke = []			
		for j in range(len(amps)):
			#Set Voltage
			act_command[i] = amps[j]
			#Apply map
			dm.actuators = act_command
			#Actuate
			dm.actuate()

			#Take image
			img = 0
			for p in range(num_frames):
				cam.take_single_exposure()
				img += cam.get_last_image()
			
			PSF = Field(img.ravel(), focal_grid)
			#If image is the same, add counter to list
			if PSF=PSF_ref:
				PSF_poke.append(j)

		if len(PSF_poke) > 40:
			print('Actuator %i may need to be looked at further' % i+1)

	#Applies zero voltage to actuators for shutdown
	dm.actuators = current_reference
	dm.actuate()

	cam.shutdown()
	vimba.shutdown()